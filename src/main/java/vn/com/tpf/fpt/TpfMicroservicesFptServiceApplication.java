package vn.com.tpf.fpt;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
@EnableJpaAuditing
@EnableDiscoveryClient
@EnableResourceServer
@Configuration
public class TpfMicroservicesFptServiceApplication {

	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("Asia/Ho_Chi_Minh"));
	}

	public static void main(String[] args) {
		SpringApplication.run(TpfMicroservicesFptServiceApplication.class, args);
	}

	@Bean
	ObjectMapper objectMapper() {
		return new ObjectMapper();
	};

	@Bean
	public RestTemplate restTemplate() {
		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setConnectionRequestTimeout(50000);
		httpRequestFactory.setConnectTimeout(50000);
		httpRequestFactory.setReadTimeout(50000);
		return new RestTemplate(httpRequestFactory);
	}

//	@Bean 
//	public Statement statement() {
//		try {
//			String myDriver = "oracle.jdbc.OracleDriver";
//			String myUrl = "jdbc:oracle:thin:@10.1.66.26:1525:ficodbuat";
//			Class.forName(myDriver);
//			Connection conn = DriverManager.getConnection(myUrl, "anhdlh", "Anh#2019");
//			Statement st = conn.createStatement();
//			return st;
//		}
//		catch (Exception e) {
//			return null;
//		}
//		
//	}

}
