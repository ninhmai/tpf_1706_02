package vn.com.tpf.fpt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

import java.util.List;


/**
 * Created by NgocIT on Mar, 2019
 */

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(exclude = {"loanDetail","addresses","photos","references","docPostApproved","returned","supplement"},callSuper=false)
@Table(name = "fpt_customers", schema = "fpt")
public class FptCustomer extends AuditableEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "app_id")
	private String appId;

	@Column(name = "cust_id")
	private Long custId;


	@Column(name = "last_name")
	private String lastName;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "middle_name")
	private String middleName;

	@Column(name = "gender")
	private String gender;

	@Column(name = "date_of_birth")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Ho_Chi_Minh")
	private Date dateOfBirth;

	@Column(name = "national_id")
	private String nationalId;

	@Column(name = "issue_date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Ho_Chi_Minh")
	private Date issueDate;
	
	@Column(name = "issue_place")
	private String issuePlace;

	@Column(name = "employee_card")
	private String employeeCard;

	@Column(name = "marital_status")
	private String maritalStatus;

	@Column(name = "mobile_phone")
	private String mobilePhone;
	
	@Column(name = "salary")
	private Long salary;

	@Column(name = "duration_year")
	private Integer durationYear;

	@Column(name = "duration_month")
	private Integer durationMonth;

	@Column(name = "map")
	private String map;

	@Column(name = "owner_national_id")
	private String ownerNationalId;

	@Column(name = "contact_address")
	private String contactAddress;

	@Column(name = "dsa_code")
	private String dsaCode;
	
	@Column(name = "company_name")
	private String companyName;
	
	@Column(name = "tax_code")
	private String taxCode;
	
	@Column(name = "status")
    private String status;
	
    @Column(name = "automation_result",columnDefinition = "TEXT")
    private String automationResult;

 
	@OneToOne(mappedBy = "fptCustomer", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private FptLoanDetail loanDetail;
	
    

	@OneToMany(mappedBy = "fptCustomer", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<FptAddress> addresses;
	
    

	@OneToMany(mappedBy = "fptCustomer", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<FptPhoto> photos ;
	


	@OneToMany(mappedBy = "fptCustomer", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<FptProductDetail> productDetails ;
	
	@OneToMany(mappedBy = "fptCustomer", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<FptReference> references ;
	

	@OneToMany(mappedBy = "fptCustomer", fetch = FetchType.LAZY)
    private List<FptDocPostApproved> docPostApproved ;
	
	

	@OneToMany(mappedBy = "fptCustomer",fetch = FetchType.LAZY)
	private List<FptReturned> returned ;


	@OneToMany(mappedBy = "fptCustomer",fetch = FetchType.LAZY)
	private List<FptSupplement> supplement ;

	

	public void setLoanDetail(FptLoanDetail loanDetail) {
		this.loanDetail = loanDetail;
		this.loanDetail.setFptCustomer(this);
	}
	
	
	public void setAddresses(List<FptAddress> addresses) {
		this.addresses = addresses;
		this.addresses.forEach(x -> x.setFptCustomer(this));
	}

	public void setPhotos(List<FptPhoto> photos) {
		this.photos = photos;
		this.photos.forEach(x -> x.setFptCustomer(this));
	}

	
	public void setProductDetails(List<FptProductDetail> productDetails) {
		this.productDetails = productDetails;
		this.productDetails.forEach(x -> x.setFptCustomer(this));
	}

	

	public void setReferences(List<FptReference> references) {
		this.references = references;
		this.references.forEach(x -> x.setFptCustomer(this));
	}


	public void setDocPostApproved(List<FptDocPostApproved> docPostApproved) {
		this.docPostApproved = docPostApproved;
		this.docPostApproved.forEach(x -> x.setFptCustomer(this));
	}

	

	public void setReturned(List<FptReturned> returned) {
		this.returned = returned;
		this.returned.forEach(x -> x.setFptCustomer(this));
	}

	

	public void setSupplement(List<FptSupplement> supplement) {
		this.supplement = supplement;
		this.supplement.forEach(x -> x.setFptCustomer(this));
	}
	
	
//	public void setReferences(List<MomoReference> references) {
//		this.references = references;
//		this.references.forEach(x -> x.setMomoCustomer(this));
//
//	}
	
	
	

	
}