package vn.com.tpf.fpt.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "finn_addresses")
public class FinnAddress {
	@Id
	@JsonIgnore
	private long id;
	
	@Column(name = "region")
	private String region;
	
	@Column(name = "city_name")
	private String cityName;

	@Column(name = "post_code")
	private String postCode;

	@Column(name = "area_name")
	private String areaName;

	@Column(name = "area_code")
	private String areaCode;

}
