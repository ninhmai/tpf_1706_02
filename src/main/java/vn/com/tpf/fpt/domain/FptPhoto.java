package vn.com.tpf.fpt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.tpf.fpt.web.response.PhotoRespone;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Created by NgocIT on Mar, 2019
 */

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fpt_photos", schema = "fpt")
public class FptPhoto extends AuditableEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "document_type")
	private String documentType;

	@Column(name = "link",columnDefinition = "TEXT")
	private String link;

	@ManyToOne
	@JoinColumn(name = "fpt_customer_id")
	@JsonIgnore
	private FptCustomer fptCustomer;
	
	
	public PhotoRespone toPhotoRespone() {
		return PhotoRespone.builder()
				.documentType(documentType).link(link)
				.build();
	}
}