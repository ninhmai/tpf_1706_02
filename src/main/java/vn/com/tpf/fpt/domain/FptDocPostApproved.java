package vn.com.tpf.fpt.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fpt_doc_post_approved", schema = "fpt")
public class FptDocPostApproved extends AuditableEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Column(name = "document_code")
	private String documentCode;

	@Column(name = "file",columnDefinition = "TEXT")
	private String file;
	
	@ManyToOne
	@JoinColumn(name = "fpt_customer_id")
	@JsonIgnore
	private FptCustomer fptCustomer;
	
}
