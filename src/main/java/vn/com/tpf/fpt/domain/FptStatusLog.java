
package vn.com.tpf.fpt.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;




/**
 * Created by NgocIT on Mar, 2019
 */

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "fpt_status_logs", schema = "fpt")
public class FptStatusLog extends AuditableEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	
	@Column(name = "cust_id")
	private Long custId;

	@Column(name = "app_id")
	private String appId;

	@Column(name = "status")
	private String status;

	@Column(name = "note", columnDefinition = "TEXT")
	private String note;

}