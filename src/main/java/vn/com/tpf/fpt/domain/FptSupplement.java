package vn.com.tpf.fpt.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fpt_supplement", schema = "fpt")
public class FptSupplement extends AuditableEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "comment_tpf")
	private String commentTpf;
	
	@Column(name = "comment_fpt",columnDefinition = "TEXT")
	private String commentFpt;
	
	@Column(name = "isPending")
	private Boolean isPending;
	
	@ManyToOne
	@JoinColumn(name = "fpt_customer_id")
	@JsonIgnore
	private FptCustomer fptCustomer;
	
}
