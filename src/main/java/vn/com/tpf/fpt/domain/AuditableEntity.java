package vn.com.tpf.fpt.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedDate;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Getter;
import lombok.Setter;
import vn.com.tpf.fpt.utils.LocalDateTimeSerializer;


/**
 * Created by NgocIT on Feb, 2019
 */

@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@MappedSuperclass
public abstract class AuditableEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	@JsonIgnore
	private long id;

	@CreatedDate
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	LocalDateTime createdDate ;

	@LastModifiedDate
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	LocalDateTime lastModifiedDate;


}
