package vn.com.tpf.fpt.domain;

import lombok.*;
import vn.com.tpf.fpt.web.response.AddressRespone;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Created by NgocIT on Mar, 2019
 */

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fpt_addresses", schema = "fpt")
public class FptAddress extends AuditableEntity  {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	@Column(name = "address_type")
	private String addressType;

	@Column(name = "address_1")
	private String address1;

	@Column(name = "address_2")
	private String address2;

	@Column(name = "ward")
	private String ward;

	@Column(name = "district")
	private String district;

	@Column(name = "province")
	private String province;
	
	@Column(name = "region")
	private String region;

	@ManyToOne
	@JoinColumn(name = "fpt_customer_id")
	@JsonIgnore
	private FptCustomer fptCustomer;
	
	public AddressRespone toAddressRespone() {
		return AddressRespone.builder()
				.addressType(addressType).address1(address1)
				.address2(address2).ward(ward)
				.district(district).province(province)
				.build();
	}

}