package vn.com.tpf.fpt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.tpf.fpt.web.response.ProductDetailRespone;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Created by NgocIT on Mar, 2019
 */

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fpt_product_details", schema = "fpt")
public class FptProductDetail extends AuditableEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "model")
	private String model;

	@Column(name = "good_code")
	private String goodCode;

	@Column(name = "good_type")
	private String goodType;

	@Column(name = "good_price")
	private Long goodPrice;

	@Column(name = "quantity")
	private Integer quantity;

	@ManyToOne
	@JoinColumn(name = "fpt_customer_id")
	@JsonIgnore
	private FptCustomer fptCustomer;
	
	public ProductDetailRespone toProductDetailRespone() {
		return ProductDetailRespone.builder()
				.model(model).goodCode(goodCode)
				.goodType(goodType).goodPrice(goodPrice).quantity(quantity).
				build();
	}
}
