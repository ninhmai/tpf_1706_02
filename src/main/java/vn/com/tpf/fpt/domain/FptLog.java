

package vn.com.tpf.fpt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;



/**
 * Created by NgocIT on Mar, 2019
 */

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fpt_logs", schema = "fpt")
public class FptLog extends AuditableEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "cust_id")
	private Long custId;
	
	@Column(name = "method")
	private String method;

	@Column(name = "data",columnDefinition = "TEXT")
	private String data;


}