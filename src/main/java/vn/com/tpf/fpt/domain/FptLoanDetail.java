package vn.com.tpf.fpt.domain;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.tpf.fpt.web.response.LoanDetailRespone;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fpt_loan_details", schema = "fpt")
public class FptLoanDetail extends AuditableEntity  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "product")
	private String product;
	

	@Column(name = "loan_amount")
	private Long loanAmount;
	

	@Column(name = "tenor")
	private Integer tenor;


	@Column(name = "annualr")
	private Double annualr;
	

	@Column(name = "down_payment")
	private Float downPayment;
	

	@Column(name = "due_date")
	private Integer dueDate;
	

	@Column(name = "emi")
	private Double emi;
	
	@Column(name = "loan_id")
	private String loanId;

	@OneToOne
	@JoinColumn(name = "fpt_customer_id")
	@JsonIgnore
	private FptCustomer fptCustomer;
	
	public LoanDetailRespone toLoanDetailRespone() {
		return LoanDetailRespone.builder()
				.product(product).loanAmount(loanAmount)
				.tenor(tenor).annualr(annualr)
				.downPayment(downPayment).dueDate(dueDate)
				.emi(emi).loanId(loanId)
				.build();
	} 
}
