package vn.com.tpf.fpt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.com.tpf.fpt.web.response.ReferenceRespone;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Created by NgocIT on Mar, 2019
 */
@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "fpt_references", schema = "fpt")
public class FptReference extends AuditableEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "full_name")
	private String fullName;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "relation")
	private String relation;

	@Column(name = "personal_id")
	private String personalId;

	@ManyToOne
	@JoinColumn(name = "fpt_customer_id")
	@JsonIgnore
	private FptCustomer fptCustomer;
	
	
	public ReferenceRespone toReferenceRespone() {
		return ReferenceRespone.builder()
				.fullName(fullName).phoneNumber(phoneNumber)
				.relation(relation).personalId(personalId)
				.build();
	}
}