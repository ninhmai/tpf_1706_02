package vn.com.tpf.fpt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;



@Component
public class WebMvcConfig implements WebMvcConfigurer {
   @Autowired
   LogApiIntercreptorHandler logApiIntercreptorHandler;

   @Override
   public void addInterceptors(InterceptorRegistry registry) {
      registry.addInterceptor(logApiIntercreptorHandler);
   }
}