package vn.com.tpf.fpt.config;

import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
public class SecurityConfig extends ResourceServerConfigurerAdapter {

//	private final String resourceId = "tpf-service-fpt";
	
	private final String resourceId = "fpt-service";
	

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		// TODO Auto-generated method stub
		resources.resourceId(resourceId);
		super.configure(resources);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().requestMatchers(EndpointRequest.toAnyEndpoint()).permitAll()
				.antMatchers(HttpMethod.POST, "/customers_json/**/update_status/**").permitAll()
				.antMatchers(HttpMethod.POST, "/customers_json/**/**/update_automation_result").permitAll()
//				.authenticated().antMatchers("/customers_pgp/**").access("#oauth2.hasScope('tpf-root') or #oauth2.hasScope('momo-service')")
				.antMatchers("/customers_json/**").access("#oauth2.hasAnyScope('tpf-service-fpt','tpf-service-app','tpf-root')")
				.antMatchers("/customers_pgp/**").access("#oauth2.hasAnyScope('3p-service-fpt','tpf-service-fpt','fpt-service','tpf-root')")
				.anyRequest().authenticated();
	}
}
