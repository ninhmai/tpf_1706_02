//package vn.com.tpf.fpt.config;
//
//
//
//import java.net.UnknownHostException;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
//import org.springframework.data.redis.serializer.StringRedisSerializer;
//
//
//import redis.clients.jedis.JedisPoolConfig;
//
//@Configuration
//public class RedisConfig {
//
//  
//
//   @Bean
//   public RedisConnectionFactory redisConnectionFactory() throws UnknownHostException {
//        JedisPoolConfig poolConfig = new JedisPoolConfig();
//	poolConfig.setMaxTotal(20);
//	poolConfig.setMinIdle(2);
//	poolConfig.setMaxIdle(5);
//
//	JedisConnectionFactory factory = new JedisConnectionFactory(poolConfig);
//	factory.setHostName("127.0.0.1");
//	factory.setUsePool(true);
//	factory.setPort(6379);
//	return factory;
//   }
//
//
//   @Bean
//   public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
//	RedisTemplate<String, Object> template = new RedisTemplate<>();
//	template.setConnectionFactory(connectionFactory);
//	template.setDefaultSerializer(new GenericJackson2JsonRedisSerializer());
//	template.setKeySerializer(new StringRedisSerializer());
//	template.setHashKeySerializer(new GenericJackson2JsonRedisSerializer());
//	template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
//	return template;
//   }
//}