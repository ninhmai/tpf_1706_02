package vn.com.tpf.fpt.config.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;
import vn.com.tpf.fpt.config.filter.extension.HttpServletRequestWrapperExtension;
import vn.com.tpf.fpt.config.filter.extension.HttpServletResponseWrapperExtension;
import vn.com.tpf.fpt.utils.pgp.PGPHelper;


@Slf4j
public class FptSecurityFilter implements Filter {
	
		
		@Autowired
		PGPHelper pgpHelper;

        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
                        throws IOException, ServletException {
        
                HttpServletRequestWrapperExtension wrappedRequest = new HttpServletRequestWrapperExtension(
                                (HttpServletRequest) request);

                HttpServletResponseWrapperExtension wrappedResponse = new HttpServletResponseWrapperExtension(
                                (HttpServletResponse) response);
                String method = wrappedRequest.getMethod();
 
                try {
                	
                	 PGPHelper pgpHelper = new PGPHelper();
                        if (method.equals("POST") ||method.equals("PUT")) {

                                ByteArrayOutputStream desStream = new ByteArrayOutputStream();
                                //IOUtils.toString(wrappedRequest.getReader())
                                pgpHelper.decryptAndVerifySignature(wrappedRequest.getReader(), desStream);
                                wrappedRequest.updateInputStream(desStream.toByteArray());
//                                log.info();  	
                   
                        }

                        chain.doFilter(wrappedRequest, wrappedResponse);
                        // response
                        String respString = wrappedResponse.getContent();
                       
                        if(!respString.trim().isEmpty()) {
	                        ByteArrayOutputStream out = new ByteArrayOutputStream();
	                        pgpHelper.encryptAndSign(respString.getBytes(), out);
	                        PrintWriter printWriter = response.getWriter();
	                        printWriter.write(out.toString());
	                        printWriter.flush();
	                        printWriter.close();
                        }
                       

                } catch (Exception ex) {
                	
                        log.error(ex.getMessage(), ex);
                }
        }

        @Override
        public void destroy() {
        }
        
        
	
}