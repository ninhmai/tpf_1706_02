package vn.com.tpf.fpt.config.filter.extension;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

import org.apache.commons.io.IOUtils;

public class HttpServletRequestWrapperExtension extends HttpServletRequestWrapper {

    private byte[] cachedContent;
    private ServletInputStreamExtension streamInput;

    public HttpServletRequestWrapperExtension(HttpServletRequest request) throws IOException {
        super(request);
        streamInput = new ServletInputStreamExtension();
      
    }

    public void resetInputStream() {
        streamInput.setStream(new ByteArrayInputStream(cachedContent));
    }
    

    public void updateInputStream(byte[] value) {
        streamInput.setStream(new ByteArrayInputStream(value));
    }
    

    @SuppressWarnings("deprecation")
	@Override
    public ServletInputStream getInputStream() throws IOException {
        if (cachedContent == null) {
            cachedContent = IOUtils.toByteArray(this.getRequest().getReader());
            streamInput.setStream(new ByteArrayInputStream(cachedContent));
        }
        return streamInput;
    }

    @SuppressWarnings("deprecation")
	@Override
    public BufferedReader getReader() throws IOException {
        if (cachedContent == null) {
            cachedContent = IOUtils.toByteArray(this.getRequest().getReader());
            streamInput.setStream(new ByteArrayInputStream(cachedContent));
        }
        return new BufferedReader(new InputStreamReader(streamInput));
    }

	

}