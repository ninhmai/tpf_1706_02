package vn.com.tpf.fpt.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import vn.com.tpf.fpt.config.filter.FptSecurityFilter;



/**
 * WebConfig
 */
@Configuration
public class WebConfig {
    @Bean
    public FilterRegistrationBean<FptSecurityFilter> momoFilter() {
        FilterRegistrationBean<FptSecurityFilter> filterRegBean = new FilterRegistrationBean<>();
        filterRegBean.setFilter(new FptSecurityFilter());
        filterRegBean.addUrlPatterns("/customers_pgp/*");
        filterRegBean.setOrder(2);
        return filterRegBean;
    }
}