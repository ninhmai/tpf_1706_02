package vn.com.tpf.fpt.utils;

import java.io.IOException;


import org.springframework.beans.factory.annotation.Autowired;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

import vn.com.tpf.fpt.web.request.LoanDetailRequest;

public class LoanDetailRequestJsonDeserializer extends JsonDeserializer<LoanDetailRequest> {


	@Autowired
	ObjectMapper objectMapper;
	
	@Override
	public LoanDetailRequest deserialize(final JsonParser p, final DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
			return objectMapper.readValue(p,LoanDetailRequest.class);
	}
}
