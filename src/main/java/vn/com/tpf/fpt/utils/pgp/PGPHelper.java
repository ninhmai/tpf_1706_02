package vn.com.tpf.fpt.utils.pgp;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;


import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.security.*;
import java.util.Date;
import java.util.Iterator;





@Slf4j
//@Component
public class PGPHelper {
    private static final int BUFFER_SIZE = 1 << 16; // should always be power of 2(one shifted bitwise 16 places)
 
    private PGPPublicKey encryptionPublicKey;
    private PGPPublicKey signaturePublicKey;
    private PGPSecretKeyRingCollection pgpSec;
    private PGPSecretKey secretKey;
    private PGPSignatureGenerator signatureGenerator;
    private char[] password;
    static {
        Security.addProvider(new BouncyCastleProvider());
    }


//    @PostConstruct
    public PGPHelper() {
    	 this.password = PGPInfo.preshareKey.toCharArray();
         InputStream pubStream = new ByteArrayInputStream(PGPInfo.publicKey.getBytes());
         InputStream priStream = new ByteArrayInputStream(PGPInfo.privateKey.getBytes());
         try {
 			readKey(pubStream, null, null, priStream);
 		} catch ( Exception ex) {
 			 log.error("PGPHelper " +ex.getMessage());
 			 new PGPException(ex.getMessage());
 		}
    	
    }

  
    private void readKey(InputStream pubStream, Long encryptpublicKeyId, Long signaturePublicKeyId, InputStream priStream) throws Exception , PGPException {
        try {
            readPublicKey(pubStream, encryptpublicKeyId, signaturePublicKeyId);
            this.pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(priStream));
            this.secretKey = readSecretKey(pgpSec);
        }
        catch (IOException | PGPException | NoSuchProviderException ex) {
        	  log.error(ex.getMessage());
        	  new PGPException(ex.getMessage());
        }
        finally {
            try {
                pubStream.close();
                priStream.close();
            }
            catch (IOException ex) {
            	  log.error(ex.getMessage());
            	  new IOException(ex.getMessage());
            }
        }
    }

  

    public void decryptAndVerifySignature(BufferedReader encryptBufferedReader, OutputStream decryptData) {
    	
    	
        try {
        	
        	byte[] encryptData = IOUtils.toString(encryptBufferedReader).getBytes();
            InputStream bais =  PGPUtil.getDecoderStream(new ByteArrayInputStream(encryptData));
            PGPObjectFactory objectFactory = new PGPObjectFactory(bais);
            
            Object firstObject = objectFactory.nextObject();
            PGPEncryptedDataList dataList = (PGPEncryptedDataList) (firstObject instanceof PGPEncryptedDataList ? firstObject : objectFactory.nextObject());
            if(dataList == null) {
            	throw new PGPException("encrypt data not correct");
            }
            Iterator<?> it = dataList.getEncryptedDataObjects();
            PGPPrivateKey privateKey = null;
            PGPPublicKeyEncryptedData encryptedData = null;
            while (privateKey == null && it.hasNext()) {
                encryptedData = (PGPPublicKeyEncryptedData) it.next();
                privateKey = findSecretKey(encryptedData.getKeyID());
            }
            if (encryptedData == null || privateKey == null) {
            	log.error("secret key for message not found.");
                throw new IllegalArgumentException("secret key for message not found.");
            }
            InputStream clear = encryptedData.getDataStream(privateKey, "BC");
            PGPObjectFactory clearObjectFactory = new PGPObjectFactory(clear);
            Object message = clearObjectFactory.nextObject();

            if (message instanceof PGPCompressedData) {
                PGPCompressedData cData = (PGPCompressedData) message;
                objectFactory = new PGPObjectFactory(cData.getDataStream());
                message = objectFactory.nextObject();
            }

            PGPOnePassSignature calculatedSignature = null;
            if (message instanceof PGPOnePassSignatureList) {
                calculatedSignature = ((PGPOnePassSignatureList) message).get(0);
                calculatedSignature.initVerify(signaturePublicKey, "BC");
                message = objectFactory.nextObject();
            }

            if (message instanceof PGPLiteralData) {
                PGPLiteralData ld = (PGPLiteralData) message;
                InputStream literalDataStream = ld.getInputStream();
                int ch;
                while ((ch = literalDataStream.read()) >= 0) {
                    if (calculatedSignature != null) {
                        calculatedSignature.update((byte) ch);
                    }
                    decryptData.write((byte) ch);
                }
            }
            else if (message instanceof PGPOnePassSignatureList) {
            	log.error("encrypted message contains a signed message - not literal data.");
                throw new PGPException("encrypted message contains a signed message - not literal data.");
            }
            else {
            	log.error("\"message is not a simple encrypted file - type unknown.");
                throw new PGPException("message is not a simple encrypted file - type unknown.");
            }

            if (calculatedSignature != null) {
                PGPSignatureList signatureList = (PGPSignatureList) objectFactory.nextObject();
                PGPSignature messageSignature = (PGPSignature) signatureList.get(0);
                if (!calculatedSignature.verify(messageSignature)) {
                	log.error("signature verification failed");
                    throw new PGPException("signature verification failed");
                }
            }

            if (encryptedData.isIntegrityProtected()) {
                if (!encryptedData.verify()) {
                	log.error("message failed integrity check");
                    throw new PGPException("message failed integrity check");
                }
            }
       

        }
        catch (IOException | IllegalArgumentException | NoSuchProviderException | PGPException | SignatureException ex) {
        	log.error(ex.getMessage());
        	 new PGPException(ex.getMessage(), ex);
        }
        
    }
    
    
 public void decryptAndVerifySignature(byte[] encryptData, OutputStream decryptData) {
    	
    	
        try {
        	log.info(encryptData.toString());	
            InputStream bais =  PGPUtil.getDecoderStream(new ByteArrayInputStream(encryptData));
            PGPObjectFactory objectFactory = new PGPObjectFactory(bais);
            
            Object firstObject = objectFactory.nextObject();
            PGPEncryptedDataList dataList = (PGPEncryptedDataList) (firstObject instanceof PGPEncryptedDataList ? firstObject : objectFactory.nextObject());
            if(dataList == null) {
            	throw new PGPException("encrypt data not correct");
            }
            Iterator<?> it = dataList.getEncryptedDataObjects();
            PGPPrivateKey privateKey = null;
            PGPPublicKeyEncryptedData encryptedData = null;
            while (privateKey == null && it.hasNext()) {
                encryptedData = (PGPPublicKeyEncryptedData) it.next();
                privateKey = findSecretKey(encryptedData.getKeyID());
            }
            if (encryptedData == null || privateKey == null) {
            	log.error("secret key for message not found.");
                throw new IllegalArgumentException("secret key for message not found.");
            }
            InputStream clear = encryptedData.getDataStream(privateKey, "BC");
            PGPObjectFactory clearObjectFactory = new PGPObjectFactory(clear);
            Object message = clearObjectFactory.nextObject();

            if (message instanceof PGPCompressedData) {
                PGPCompressedData cData = (PGPCompressedData) message;
                objectFactory = new PGPObjectFactory(cData.getDataStream());
                message = objectFactory.nextObject();
            }

            PGPOnePassSignature calculatedSignature = null;
            if (message instanceof PGPOnePassSignatureList) {
                calculatedSignature = ((PGPOnePassSignatureList) message).get(0);
                calculatedSignature.initVerify(signaturePublicKey, "BC");
                message = objectFactory.nextObject();
            }

            if (message instanceof PGPLiteralData) {
                PGPLiteralData ld = (PGPLiteralData) message;
                InputStream literalDataStream = ld.getInputStream();
                int ch;
                while ((ch = literalDataStream.read()) >= 0) {
                    if (calculatedSignature != null) {
                        calculatedSignature.update((byte) ch);
                    }
                    decryptData.write((byte) ch);
                }
            }
            else if (message instanceof PGPOnePassSignatureList) {
            	log.error("encrypted message contains a signed message - not literal data.");
                throw new PGPException("encrypted message contains a signed message - not literal data.");
            }
            else {
            	log.error("\"message is not a simple encrypted file - type unknown.");
                throw new PGPException("message is not a simple encrypted file - type unknown.");
            }

            if (calculatedSignature != null) {
                PGPSignatureList signatureList = (PGPSignatureList) objectFactory.nextObject();
                PGPSignature messageSignature = (PGPSignature) signatureList.get(0);
                if (!calculatedSignature.verify(messageSignature)) {
                	log.error("signature verification failed");
                    throw new PGPException("signature verification failed");
                }
            }

            if (encryptedData.isIntegrityProtected()) {
                if (!encryptedData.verify()) {
                	log.error("message failed integrity check");
                    throw new PGPException("message failed integrity check");
                }
            }
       

        }
        catch (IOException | IllegalArgumentException | NoSuchProviderException | PGPException | SignatureException ex) {
        	log.error(ex.getMessage());
        	 new PGPException(ex.getMessage(), ex);
        }
    }

    private void readPublicKey(InputStream in, Long publicKeyId, Long signaturePublicKeyId) throws IOException, PGPException {
        in = PGPUtil.getDecoderStream(in);
        PGPPublicKeyRingCollection pkCol = new PGPPublicKeyRingCollection(in);
        PGPPublicKeyRing pkRing;
        Iterator<?> it = pkCol.getKeyRings();
        if (publicKeyId == null || publicKeyId == -1) {
            while (it.hasNext()) {
                pkRing = (PGPPublicKeyRing) it.next();
                Iterator<?> pkIt = pkRing.getPublicKeys();
                while (pkIt.hasNext()) {
                    PGPPublicKey key = (PGPPublicKey) pkIt.next();
                    if (key.isEncryptionKey()) {
                        encryptionPublicKey = key;
                        break;
                    }
                }
            }

        }
        else {
            encryptionPublicKey = pkCol.getPublicKey(publicKeyId);
        }
        if (encryptionPublicKey == null) {
            throw new PGPException("Invalid public Key");
        }
        if (signaturePublicKeyId == null || signaturePublicKeyId == -1) {
            signaturePublicKey = encryptionPublicKey;
        }
        else {
            signaturePublicKey = pkCol.getPublicKey(signaturePublicKeyId);
        }
    }

    public void encryptAndSign(byte[] data, OutputStream out) throws Exception {
        try {
            out = new ArmoredOutputStream(out);
            PGPEncryptedDataGenerator encryptedDataGenerator = new PGPEncryptedDataGenerator(PGPEncryptedDataGenerator.CAST5, new SecureRandom(), "BC");
            encryptedDataGenerator.addMethod(encryptionPublicKey);
            PGPCompressedDataGenerator comData = null;
            try (OutputStream encryptedOut = encryptedDataGenerator.open(out, new byte[BUFFER_SIZE])) {
                comData = new PGPCompressedDataGenerator(PGPCompressedDataGenerator.ZIP);
                try (OutputStream compressedOut = comData.open(encryptedOut)) {
                    PGPSignatureGenerator pgpsg = createSignatureGenerator();
                    pgpsg.generateOnePassVersion(false).encode(compressedOut);
                    writeToLiteralData(pgpsg, compressedOut, data);
                    pgpsg.generate().encode(compressedOut);
                }
            }

            finally {
                if (comData != null) {
                    try {
                        comData.close();
                    }
                    catch (IOException ex) {
                        //NO OP
                    }
                }
                try {
                    encryptedDataGenerator.close();
                }
                catch (IOException ex) {
                    //NO OP
                }
                out.close();
            }
        }
        catch (IOException | NoSuchAlgorithmException | NoSuchProviderException | PGPException | SignatureException ex) {
//            throw new Exception(ex.getMessage(), ex);
        }
    }

    private PGPSignatureGenerator createSignatureGenerator() throws NoSuchProviderException, NoSuchAlgorithmException, PGPException {
        if (signatureGenerator == null) {
            PGPPrivateKey pgpPrivKey = secretKey.extractPrivateKey(password, "BC");
            PGPPublicKey internalPublicKey = secretKey.getPublicKey();
            PGPSignatureGenerator generator = new PGPSignatureGenerator(internalPublicKey.getAlgorithm(), HashAlgorithmTags.SHA1, "BC");
            generator.initSign(PGPSignature.BINARY_DOCUMENT, pgpPrivKey);
            for (Iterator<?> i = internalPublicKey.getUserIDs(); i.hasNext();) {
                String userId = (String) i.next();
                PGPSignatureSubpacketGenerator spGen = new PGPSignatureSubpacketGenerator();
                spGen.setSignerUserID(false, userId);
                generator.setHashedSubpackets(spGen.generate());
                break;
            }
            this.signatureGenerator = generator;
        }
        return signatureGenerator;
    }

   

    private PGPPrivateKey findSecretKey(long keyID) throws IOException, PGPException, NoSuchProviderException {
        PGPSecretKey pgpSecKey = pgpSec.getSecretKey(keyID);
        if (pgpSecKey == null) {
            return null;
        }
        return pgpSecKey.extractPrivateKey(password, "BC");
    }

    private static void writeToLiteralData(PGPSignatureGenerator signatureGenerator, OutputStream out, byte[] data) throws IOException, SignatureException {
        PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
        ByteArrayInputStream contentStream = new ByteArrayInputStream(data);
        try (OutputStream literalOut = lData.open(out, PGPLiteralData.BINARY, "pgp", new Date(), new byte[BUFFER_SIZE])) {
            byte[] buf = new byte[BUFFER_SIZE];
            int len;
            while ((len = contentStream.read(buf, 0, buf.length)) > 0) {
                literalOut.write(buf, 0, len);
                signatureGenerator.update(buf, 0, len);
            }
        }
        finally {
            lData.close();
        }
    }

    private PGPSecretKey readSecretKey(PGPSecretKeyRingCollection collection) throws IOException, PGPException, NoSuchProviderException {
        Iterator<?> it = collection.getKeyRings();
        PGPSecretKeyRing pbr;
        while (it.hasNext()) {
            Object readData = it.next();
            if (readData instanceof PGPSecretKeyRing) {
                pbr = (PGPSecretKeyRing) readData;
                return pbr.getSecretKey();
            }
        }
        log.error("secret key for message not found.");
        throw new IllegalArgumentException("secret key for message not found.");
    }
}
