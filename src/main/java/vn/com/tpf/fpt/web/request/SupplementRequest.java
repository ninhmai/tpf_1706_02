package vn.com.tpf.fpt.web.request;




import lombok.Getter;

import vn.com.tpf.fpt.domain.FptSupplement;

@Getter
public class SupplementRequest  extends BaseRequest  {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	
	private String comment;
	
	public FptSupplement tpfAddSupplement() {
		return FptSupplement.builder()
				.code(code)
				.commentTpf(comment)
				.commentFpt(null)
				.isPending(true)
				.build();
	}
	

}
