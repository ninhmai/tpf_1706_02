package vn.com.tpf.fpt.web.request;


import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;



import com.fasterxml.jackson.annotation.JsonCreator;

import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Getter;
import lombok.Setter;
import vn.com.tpf.fpt.domain.FptAddress;



@Getter
@Setter
public class AddressRequest  extends BaseRequest  {


	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@NotNull
	private AddressTypes addressType;
	
	@NotEmpty
	private String address1;
	
	@NotEmpty
	private String address2;

	@NotEmpty
	private String ward;
	
	@NotEmpty
	private String district;
	
	@NotEmpty
	private String province;
	
	private String region;
	
	
	public FptAddress toFptAddress() {

		return FptAddress.builder()
				 .addressType(addressType.toValue())
				.address1(address1).address2(address2)
				.ward(ward).district(district).province(province)
				.region(region)
				.build();
	}
	
	private enum AddressTypes {
		
		FamilyBookAddress,
		CurrentAddress,
		SpouseAddress,
		WorkingAddress;

	    private static Map<String, AddressTypes> namesMap = new HashMap<String, AddressTypes>(4);

	    static {
	        namesMap.put("Family Book Address", FamilyBookAddress);
	        namesMap.put("Current Address", CurrentAddress);
	        namesMap.put("Spouse Address", SpouseAddress);
	        namesMap.put("Working Address", WorkingAddress);
	        
	    }

	    @JsonCreator
	    public static AddressTypes forValue(String value) {
	        return namesMap.get(value);
	    }

	    @JsonValue
	    public String toValue() {
	        for (Map.Entry<String, AddressTypes> entry : namesMap.entrySet()) {
	            if (entry.getValue() == this)
	                return entry.getKey();
	        }
	       return null;// or fail
	    }

		
	}
}
