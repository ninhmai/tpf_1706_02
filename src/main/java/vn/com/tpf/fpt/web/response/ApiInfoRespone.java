package vn.com.tpf.fpt.web.response;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ApiInfoRespone {
	
	@Builder.Default
	private String time = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
	
	private String  messages;

	@Builder.Default
	private List<String> errors = new ArrayList<String>();
	
}
