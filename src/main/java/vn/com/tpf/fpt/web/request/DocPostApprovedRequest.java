package vn.com.tpf.fpt.web.request;

import javax.validation.constraints.NotEmpty;

import lombok.Getter;
import vn.com.tpf.fpt.domain.FptDocPostApproved;

@Getter
public class DocPostApprovedRequest extends BaseRequest {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@NotEmpty
	private String documentCode;
	@NotEmpty
	private String file;
	
	public FptDocPostApproved toFptDocPostApproved() {
		return FptDocPostApproved.builder()
				.documentCode(documentCode).file(file)
				.build();
	}
	

}
