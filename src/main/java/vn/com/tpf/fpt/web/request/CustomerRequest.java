package vn.com.tpf.fpt.web.request;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Getter;
import lombok.Setter;
import vn.com.tpf.fpt.domain.FptAddress;
import vn.com.tpf.fpt.domain.FptCustomer;
import vn.com.tpf.fpt.domain.FptPhoto;
import vn.com.tpf.fpt.domain.FptProductDetail;
import vn.com.tpf.fpt.domain.FptReference;
import vn.com.tpf.fpt.utils.DateAndTimeDeserialize;
import vn.com.tpf.fpt.utils.ListAddressRequestJsonDeserializer;
import vn.com.tpf.fpt.utils.ListPhotoRequestJsonDeserializer;
import vn.com.tpf.fpt.utils.ListProductDetailRequestJsonDeserializer;
import vn.com.tpf.fpt.utils.ListReferenceRequestJsonDeserializer;
//import vn.com.tpf.fpt.web.request.ReferenceRequest.ReferenceTypes;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class CustomerRequest extends BaseRequest {

	private static final long serialVersionUID = -3716069763164594245L;

	@NotNull
	private Long custId;

	@NotEmpty
	private String lastName;

	@NotEmpty
	private String firstName;
	
	@NotEmpty
	private String middleName;

	@NotNull
	private Genders gender;

	@NotNull
	@JsonDeserialize(using = DateAndTimeDeserialize.class)
	private Date dateOfBirth;

	@Size(min = 9, max = 12)
	@NotBlank
	@NotEmpty
	private String nationalId;

	@NotNull
	@JsonDeserialize(using = DateAndTimeDeserialize.class)
	private Date issueDate; 

	@NotEmpty
	private String issuePlace;

	@NotEmpty
//	@Pattern(regexp = "^[0-9]*")
	private String employeeCard;

	@NotNull
	private MaritalStatus maritalStatus;

	@Pattern(regexp = "^[1-9][0-9]*")
	@NotEmpty
	private String mobilePhone;
	
//	@NotNull
//	@Min(value = 3000000)
	@SuppressWarnings("deprecation")
	private Long salary = new Long(0);

	@NotNull
	private Integer durationYear;

	@NotNull
	private Integer durationMonth;

	@NotEmpty
	@Length(max=100)
	private String map;

	@NotEmpty
	private String ownerNationalId;

	@NotEmpty
	private String contactAddress;

	@NotEmpty
	private String companyName;

	@NotEmpty
	private String taxCode;

	@NotEmpty
	private String dsaCode;

	@Valid
	@NotNull
	private LoanDetailRequest loanDetail = new LoanDetailRequest();

	@JsonDeserialize(using = ListAddressRequestJsonDeserializer.class)
	@Valid
	@NotNull
	private List<AddressRequest> addresses = new ArrayList<AddressRequest>();

	@JsonDeserialize(using = ListPhotoRequestJsonDeserializer.class)
	@Valid
	@NotNull
	private List<PhotoRequest> photos = new ArrayList<PhotoRequest>();

	@JsonDeserialize(using = ListProductDetailRequestJsonDeserializer.class)
	@Valid
	@NotNull
	private List<ProductDetailRequest> productDetails = new ArrayList<ProductDetailRequest>();

	@JsonDeserialize(using = ListReferenceRequestJsonDeserializer.class)
	@Valid
	@NotNull
	private List<ReferenceRequest> references = new ArrayList<ReferenceRequest>();
	
	

	public FptCustomer toFptCustomer() {
		
		List<FptAddress> fptAddresses = new ArrayList<FptAddress>();
		for (AddressRequest item : this.addresses)
		{	
			FptAddress address = item.toFptAddress();
			if(address != null)
				fptAddresses.add(item.toFptAddress());
		}

		List<FptPhoto> fptPhotos = new ArrayList<FptPhoto>();
		for (PhotoRequest item : this.photos)
			fptPhotos.add(item.toFptPhoto());

		List<FptProductDetail> fptProductDetails = new ArrayList<FptProductDetail>();
		for (ProductDetailRequest item : this.productDetails)
			if(!item.getModel().isEmpty())
				fptProductDetails.add(item.toFptProductDetail());
		List<FptReference> fptReferences = new ArrayList<FptReference>();
		for (ReferenceRequest item : this.references)
			fptReferences.add(item.toFptReference());

		FptCustomer customer = FptCustomer.builder().custId(custId).lastName(firstName).firstName(lastName)
				.middleName(middleName).gender(gender.toValue()).dateOfBirth(dateOfBirth).nationalId(nationalId)
				.issueDate(issueDate).issuePlace(issuePlace.trim()).employeeCard(employeeCard)
				.maritalStatus(maritalStatus.toValue()).mobilePhone(mobilePhone).salary(salary).durationYear(durationYear)
				.durationMonth(durationMonth).map(map).ownerNationalId(ownerNationalId).contactAddress(contactAddress)
				.dsaCode(dsaCode).companyName(companyName).taxCode(taxCode).build();	
		customer.setLoanDetail(this.loanDetail.toFptLoanDetail());
		customer.setAddresses(fptAddresses);
		customer.setPhotos(fptPhotos);
		customer.setProductDetails(fptProductDetails);
		customer.setReferences(fptReferences);

		return customer;
		
	}

	private enum MaritalStatus {

		DivorcedWidow, Married, Single;

		private static Map<String, MaritalStatus> namesMap = new HashMap<String, MaritalStatus>(3);

		static {
			namesMap.put("Divorced/Widow", DivorcedWidow);
			namesMap.put("Married", Married);
			namesMap.put("Single", Single);

		}

		@JsonCreator
		public static MaritalStatus forValue(String value) {
			return namesMap.get(value);
		}

		@JsonValue
		public String toValue() {
			for (Map.Entry<String, MaritalStatus> entry : namesMap.entrySet()) {
				if (entry.getValue() == this)
					return entry.getKey();
			}
			return null;// or fail
		}

	}

	private enum Genders {

		Male, Female;

		private static Map<String, Genders> namesMap = new HashMap<String, Genders>(2);

		static {
			namesMap.put("Male", Male);
			namesMap.put("Female", Female);

		}

		@JsonCreator
		public static Genders forValue(String value) {
			return namesMap.get(value);
		}

		@JsonValue
		public String toValue() {
			for (Map.Entry<String, Genders> entry : namesMap.entrySet()) {
				if (entry.getValue() == this)
					return entry.getKey();
			}
			return null;// or fail
		}

	}

}
