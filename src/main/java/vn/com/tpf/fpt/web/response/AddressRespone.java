package vn.com.tpf.fpt.web.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AddressRespone  {


	private String addressType;
	

	private String address1;
	
	
	private String address2;

	
	private String ward;
	

	private String district;
	

	private String province;
	
//	private String region;
	
}
