package vn.com.tpf.fpt.web.request;


import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Getter;

import vn.com.tpf.fpt.domain.FptReference;

@Getter
public class ReferenceRequest  extends BaseRequest  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;	
	

	@NotEmpty
	private String fullName;
		
	@NotEmpty
	@Pattern(regexp = "^[1-9][0-9]*")
	private String phoneNumber;
	
	@NotNull
	private ReferenceTypes relation;
	
	
	private String personalId;
	
	
	public FptReference toFptReference() {
		return FptReference.builder()
				.fullName(fullName)
				.phoneNumber(phoneNumber).relation(relation.toValue()).personalId(personalId)
				.build();
	}
	
	private enum ReferenceTypes {

		Spouse, Relative, Colleague;

		private static Map<String, ReferenceTypes> namesMap = new HashMap<String, ReferenceTypes>(3);

		static {
			namesMap.put("Spouse", Spouse);
			namesMap.put("Relative", Relative);
			namesMap.put("Colleague", Colleague);
			

		}

		@JsonCreator
		public static ReferenceTypes forValue(String value) {
			return namesMap.get(value);
		}

		@JsonValue
		public String toValue() {
			for (Map.Entry<String, ReferenceTypes> entry : namesMap.entrySet()) {
				if (entry.getValue() == this)
					return entry.getKey();
			}
			return null;// or fail
		}

	}

}
