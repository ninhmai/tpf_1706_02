package vn.com.tpf.fpt.web.exception;

/**
 * Create by duong.tq
 */
public class FptAddressNotFoundException extends BaseException {

	private static final long serialVersionUID = 2715363440500672381L;
	public final int index;
	public FptAddressNotFoundException(int index) {
		this.index =index;
	}

}