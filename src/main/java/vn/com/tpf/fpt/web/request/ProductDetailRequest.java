package vn.com.tpf.fpt.web.request;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Getter;

import vn.com.tpf.fpt.domain.FptProductDetail;


@Getter
public class ProductDetailRequest  extends BaseRequest {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@NotNull
	private String model;
	
	@NotNull
	private String goodCode;
	
	@NotNull
	private GoodTypes goodType;
	
	@NotNull
	private Long goodPrice;
	
	@NotNull
	private Integer quantity;
	
	public FptProductDetail toFptProductDetail() {
		return FptProductDetail.builder()
				.goodCode(goodCode).goodType(goodType.toValue())
				.goodPrice(goodPrice).quantity(quantity)
				.model(model)
				.build();
	}
	
	

	private enum GoodTypes {

		Portable, NonPortable,None;

		private static Map<String, GoodTypes> namesMap = new HashMap<String, GoodTypes>(2);

		static {
			namesMap.put("Portable", Portable);
			namesMap.put("Non-portable", NonPortable);
			namesMap.put("", None);
		}

		@JsonCreator
		public static GoodTypes forValue(String value) {
			return namesMap.get(value);
		}

		@JsonValue
		public String toValue() {
			for (Map.Entry<String, GoodTypes> entry : namesMap.entrySet()) {
				if (entry.getValue() == this)
					return entry.getKey();
			}
			return null;// or fail
		}

	}
}
