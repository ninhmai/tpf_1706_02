package vn.com.tpf.fpt.web.exception;

public class FptSupplementException extends BaseException {

	private static final long serialVersionUID = 2715363440500672381L;

	public FptSupplementException() {

	}
	public FptSupplementException( String messages) {
		super(messages);
	}
	
	
//	public MomoNotFoundException(String requestId,String referenceId, String entity, Integer momo_loan_id) {
//		super(requestId,referenceId,entity.toUpperCase() + " Momo Loan Id " + momo_loan_id + " Not Found.");
//	}
//
//	public MomoNotFoundException(String requestId,String referenceId,String entity, long id) {
//		super(requestId,referenceId,entity.toUpperCase() + " : " + id + " Not Found.");
//	}


}