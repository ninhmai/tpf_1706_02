package vn.com.tpf.fpt.web.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import vn.com.tpf.fpt.domain.FptLoanDetail;


@Getter
public class LoanDetailRequest extends BaseRequest  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@NotEmpty
	private String product;
	
	
	@NotNull
	private Long loanAmount;
	
	@NotNull
	private Integer tenor;
	
	
	@NotNull
	private Double annualr;

	@NotNull
	private Float downPayment;
	
//	@NotNull
	private Integer dueDate;
	
	private Double emi;
	
	private String loanId;
	
//	private Double EMI;
//		
//		//BOOKING LOAN
//	private String loanId;
		
	
	public FptLoanDetail toFptLoanDetail() {
		System.out.println("start update loan detail");
		return FptLoanDetail.builder()
				.product(product).loanAmount(loanAmount).tenor(tenor)
				.annualr(annualr).downPayment(downPayment).dueDate(dueDate)
				.emi(emi).loanId(loanId)
				.build();
	}

}
