package vn.com.tpf.fpt.web.exception;

/**
 * Create by duong.tq
 */
public class FptExistException extends BaseException {

	private static final long serialVersionUID = 2715363440500672381L;

	public FptExistException() {

	}
	
	public FptExistException( long id) {
		super("Fpt cust id " + id + " đã tồn tại!");
	}

//	public MomoExistException(String requestId,String referenceId, String entity, Integer momo_loan_id) {
//		super(requestId,referenceId,entity.toUpperCase() + " Momo Loan Id " + momo_loan_id + " Exist.");
//	}
//
//	public MomoExistException(String requestId,String referenceId,String entity, long id) {
//		super(requestId,referenceId,entity.toUpperCase() + " : " + id + " Exist.");
//	}
}