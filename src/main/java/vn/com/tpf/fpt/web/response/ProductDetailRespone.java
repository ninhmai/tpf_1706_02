package vn.com.tpf.fpt.web.response;


import lombok.Builder;
import lombok.Getter;


@Builder
@Getter
public class ProductDetailRespone {


	private String model;
	

	private String goodCode;
	

	private String goodType;
	

	private Long goodPrice;
	

	private Integer quantity;

}
