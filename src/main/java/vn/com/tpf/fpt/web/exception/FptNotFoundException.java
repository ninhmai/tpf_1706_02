package vn.com.tpf.fpt.web.exception;

/**
 * Create by duong.tq
 */
public class FptNotFoundException extends BaseException {

	private static final long serialVersionUID = 2715363440500672381L;

	public FptNotFoundException() {

	}
	
	public FptNotFoundException(long cust_id) {
		super("Fpt cust Id " + cust_id + " không tồn tại!");
	}
	
	public FptNotFoundException(String app_id) {
		super("Fpt app Id " + app_id + " không tồn tại!");
	}
//	public MomoNotFoundException(String requestId,String referenceId, String entity, Integer momo_loan_id) {
//		super(requestId,referenceId,entity.toUpperCase() + " Momo Loan Id " + momo_loan_id + " Not Found.");
//	}
//
//	public MomoNotFoundException(String requestId,String referenceId,String entity, long id) {
//		super(requestId,referenceId,entity.toUpperCase() + " : " + id + " Not Found.");
//	}


}