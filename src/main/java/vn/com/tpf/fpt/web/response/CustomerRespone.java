package vn.com.tpf.fpt.web.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Builder;
import lombok.Getter;
import vn.com.tpf.fpt.domain.FptCustomer;

@Builder
@Getter
public class CustomerRespone {

	private Long custId;
	
	private String appId;

	private String lastName;

	private String firstName;

	private String middleName;

	private String gender;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Ho_Chi_Minh")
	private Date dateOfBirth;

	private String nationalId;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy",timezone = "Asia/Ho_Chi_Minh")
	private Date issueDate;

	private String issuePlace;

	private String employeeCard;

	private String maritalStatus;

	private String mobilePhone;
	
	private Long salary;

	private Integer durationYear;

	private Integer durationMonth;

	private String map;
	


	private String ownerNationalId;

	private String contactAddress;

	private String companyName;

	private String taxCode;

	private String dsaCode;
	
	
	public void setAddresses(List<AddressRespone> addresses) {
        this.addresses = addresses;
    }
	
	public void setIssuePlace(String issuePlace) {
        this.issuePlace = issuePlace;
    }

	@Builder.Default
	private LoanDetailRespone loanDetail = new LoanDetailRespone();

	@Builder.Default
	private List<AddressRespone> addresses = new ArrayList<AddressRespone>();

	@Builder.Default
	private List<PhotoRespone> photos = new ArrayList<PhotoRespone>();

	@Builder.Default
	private List<ProductDetailRespone> productDetails = new ArrayList<ProductDetailRespone>();

	@Builder.Default
	private List<ReferenceRespone> references = new ArrayList<ReferenceRespone>();

	public static CustomerRespone fromFptCustomer(FptCustomer customer) {
		List<AddressRespone> addresses = new ArrayList<AddressRespone>();
		List<PhotoRespone> photos = new ArrayList<PhotoRespone>();
		List<ReferenceRespone> references = new ArrayList<ReferenceRespone>();
		List<ProductDetailRespone> productDetails = new ArrayList<ProductDetailRespone>();
		customer.getAddresses().forEach((address) -> addresses.add(address.toAddressRespone()));
		customer.getPhotos().forEach((photo) -> photos.add(photo.toPhotoRespone()));
		customer.getProductDetails().forEach((product) -> productDetails.add(product.toProductDetailRespone()));
		customer.getReferences().forEach((reference) -> references.add(reference.toReferenceRespone()));
	
		
		return CustomerRespone.builder()
				.custId(customer.getCustId())
				.appId(customer.getAppId())
				.lastName(customer.getFirstName())
				.firstName(customer.getLastName())
				.middleName(customer.getMiddleName())
				.gender(customer.getGender())
				.dateOfBirth(customer.getDateOfBirth())
				.nationalId(customer.getNationalId())
				.issueDate(customer.getIssueDate())
				.issuePlace(customer.getIssuePlace())
				.employeeCard(customer.getEmployeeCard())
				.maritalStatus(customer.getMaritalStatus())
				.mobilePhone(customer.getMobilePhone())
				.salary(customer.getSalary())
				.durationYear(customer.getDurationYear())
				.durationMonth(customer.getDurationMonth())
				.map(customer.getMap())
				.ownerNationalId(customer.getOwnerNationalId())
				.contactAddress(customer.getContactAddress())
				.companyName(customer.getCompanyName())
				.taxCode(customer.getTaxCode())
				.dsaCode(customer.getDsaCode())
				.addresses(addresses)
				.productDetails(productDetails)
				.photos(photos)
				.references(references)
				.loanDetail(customer.getLoanDetail().toLoanDetailRespone())
				.build();
	}

}
