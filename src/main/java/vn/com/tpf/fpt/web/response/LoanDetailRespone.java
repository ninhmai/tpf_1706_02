package vn.com.tpf.fpt.web.response;




import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class LoanDetailRespone  {
	
	private String product;
		
	private Long loanAmount;
	
	private Integer tenor;

	private Double annualr;

	private Float downPayment;

	
	private Integer dueDate;
	
	private Double emi;

	private String loanId;


}
