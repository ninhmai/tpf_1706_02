package vn.com.tpf.fpt.web.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;



@Getter 
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PhotoRespone {

	private String documentType;

	private String link;

}
