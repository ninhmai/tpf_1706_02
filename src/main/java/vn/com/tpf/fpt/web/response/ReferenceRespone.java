package vn.com.tpf.fpt.web.response;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReferenceRespone  {

	private String fullName;		

	private String phoneNumber;
	
	private String relation;
		
	private String personalId;
	
}
