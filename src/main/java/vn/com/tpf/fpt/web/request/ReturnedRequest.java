package vn.com.tpf.fpt.web.request;




import lombok.Getter;

import vn.com.tpf.fpt.domain.FptReturned;

@Getter
public class ReturnedRequest  extends BaseRequest  {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String code;
	
	private String comment;
	
	public FptReturned tpfAddReturned() {
		return FptReturned.builder()
				.code(code)
				.commentTpf(comment)
				.commentFpt(null)
				.isPending(true)
				.build();
	}
	

}
