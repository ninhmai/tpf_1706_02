package vn.com.tpf.fpt.web.request;

import java.io.Serializable;


import lombok.Getter;



/**
 * BaseRequest
 */

@Getter
public abstract class BaseRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
}