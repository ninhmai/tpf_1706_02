package vn.com.tpf.fpt.web.controllers;

//import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
//import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import lombok.Builder;
import lombok.Getter;
import lombok.var;
import lombok.extern.slf4j.Slf4j;
import vn.com.tpf.fpt.domain.FinnAddress;
import vn.com.tpf.fpt.domain.FptCustomer;
import vn.com.tpf.fpt.domain.FptDocPostApproved;
import vn.com.tpf.fpt.domain.FptLoanDetail;
import vn.com.tpf.fpt.domain.FptLog;
import vn.com.tpf.fpt.domain.FptReturned;
import vn.com.tpf.fpt.domain.FptStatusLog;
import vn.com.tpf.fpt.domain.FptSupplement;
import vn.com.tpf.fpt.repositories.FinnAddressRepository;
import vn.com.tpf.fpt.repositories.FptCustomerRepository;
import vn.com.tpf.fpt.repositories.FptDocPostApprovedRepository;
import vn.com.tpf.fpt.repositories.FptLogRepository;
import vn.com.tpf.fpt.repositories.FptReturnedRepository;
import vn.com.tpf.fpt.repositories.FptStatusLogRepository;
import vn.com.tpf.fpt.repositories.FptSupplementRepository;
import vn.com.tpf.fpt.utils.pgp.PGPHelper;
import vn.com.tpf.fpt.web.exception.FptAddressNotFoundException;
import vn.com.tpf.fpt.web.exception.FptExistException;
import vn.com.tpf.fpt.web.exception.FptMethodArgumentNotValidException;
import vn.com.tpf.fpt.web.exception.FptNotFoundException;
import vn.com.tpf.fpt.web.exception.FptReturnedException;
import vn.com.tpf.fpt.web.exception.FptStatusException;
import vn.com.tpf.fpt.web.exception.FptStatusNotMatchException;
import vn.com.tpf.fpt.web.exception.FptSupplementException;
import vn.com.tpf.fpt.web.request.AddressRequest;
import vn.com.tpf.fpt.web.request.CustomerRequest;
import vn.com.tpf.fpt.web.request.DocPostApprovedRequest;
import vn.com.tpf.fpt.web.request.ReturnedRequest;
import vn.com.tpf.fpt.web.request.SupplementRequest;
import vn.com.tpf.fpt.web.response.AddressRespone;
import vn.com.tpf.fpt.web.response.CustomerRespone;

/**
 * Created by NgocIT on Mar, 2019
 */

@RestController
@Slf4j
public class FptCustomerController {

	private final String urlNodeService = "http://10.131.21.27:3333"; // UAT

// 	private final String urlNodeService = "http://10.131.21.27:3000";   //PRODUCT
// 	private final String urlAutomationService ="http://10.1.66.28";  // UAT
	private final String urlAutomationService = "http://localhost";
	private final String localhost = "localhost";
	private final String urlFptApi = "https://outsidepaymenttest.tpb.vn/fpttpbank"; // UAT
// 	private final String urlFptApi = "https://payment.tpb.vn/services/ficofpt" ; //PRODUCT

//	@Value("${url.node}")
//	private  String urlNodeService;

//	@Value("${url.automation}")
//	private String urlAutomationService;
//
//	@Value("${url.fpt}")
//	private String urlFptApi;
//	
//	@Value("${url.localhost}")
//	private String localhost;

	@Autowired
	ObjectMapper objectMapper;

	@Autowired
	private FinnAddressRepository finnAddressRepository;

	@Autowired
	private FptCustomerRepository fptCustomerRepository;
	
	@Autowired
	private FptStatusLogRepository fptStatusLogRepository;
	
	@Autowired
	private FptDocPostApprovedRepository fptDocPostApprovedRepository;

	@Autowired
	private FptReturnedRepository fptReturnedRepository;

	@Autowired
	private FptSupplementRepository fptSupplementRepository;

	@Autowired
	private FptLogRepository fptLogRepository;

	@Autowired
	RestTemplate restTemplate;

	@GetMapping("/{router_type}")
	public ResponseEntity<?> all() {
		return ResponseEntity.ok(this.fptCustomerRepository.findAll());
	}

	@GetMapping("/{router_type}/pass_automation")
	public ResponseEntity<?> allPassAutomation() {
		return ResponseEntity.ok(this.fptCustomerRepository.findFptCustomerPassAutomation());
	}

	@GetMapping("/{router_type}/failed_automation")
	public ResponseEntity<?> allFailedAutomation() {
		return ResponseEntity.ok(this.fptCustomerRepository.findFptCustomerFailedAutomation());
	}

	@GetMapping("/{router_type}/document_check")
	public ResponseEntity<?> allDocumentCheck() {
		return ResponseEntity.ok(this.fptCustomerRepository.findFptCustomerForDocumentCheck());
	}

	@GetMapping("/{router_type}/under_writing")
	public ResponseEntity<?> allUnderWriting() {
		return ResponseEntity.ok(this.fptCustomerRepository.findFptCustomerForUnderWriting());
	}

	@GetMapping("/{router_type}/loan_check")
	public ResponseEntity<?> allLoanCheck() {
		return ResponseEntity.ok(this.fptCustomerRepository.findFptCustomerForLoanCheck());
	}

	@GetMapping("/{router_type}/logs/{cust_id}")
	public ResponseEntity<?> logs(@PathVariable("cust_id") Long cust_id) {
		return ResponseEntity.ok(this.fptLogRepository.findFptLogByCustId(cust_id));
	}

	@GetMapping("/{router_type}/find_customers")
	public ResponseEntity<?> allByStatus(@RequestParam(required = true) AppStatus app_status) {
		return ResponseEntity.ok(this.fptCustomerRepository.findFptCustomerStatus(String.valueOf(app_status)));
	}

	@GetMapping("/{router_type}/find_customers_by_list_status")
	public ResponseEntity<?> allByListStatus(@RequestParam(required = true) List<String> list_app_status) {
		Set<String> listStatus = new HashSet<String>();
		list_app_status.forEach((status) -> listStatus.add(String.valueOf(status)));
		return ResponseEntity.ok(this.fptCustomerRepository.findFptCustomerByListStatus(listStatus));
	}

	@GetMapping("/{router_type}/{cust_id}")
	public ResponseEntity<?> getByCustId(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id) throws FptNotFoundException, JsonProcessingException {
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));
		return ok(fptCustomer);
	}

	@GetMapping("/{router_type}/app_id/{app_id}")
	public ResponseEntity<?> getByAppId(@PathVariable("router_type") RouterType router_type,
			@PathVariable("app_id") String app_id) throws FptNotFoundException, JsonProcessingException {
		List<FptCustomer> listFptCustomer = this.fptCustomerRepository.findFptCustomerByAppId(app_id);
		if (listFptCustomer.size() == 0)
			new FptNotFoundException(app_id);
		return ok(listFptCustomer.get(0));
	}

	@GetMapping("/{router_type}/{cust_id}/acca")
	public ResponseEntity<?> geACCA(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id) throws FptNotFoundException, JsonProcessingException {
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));
		CustomerRespone respone = CustomerRespone.fromFptCustomer(fptCustomer);
		List<AddressRespone> addresses = new ArrayList<AddressRespone>();
		respone.getAddresses().forEach((address) -> {
			List<FinnAddress> listFinnAddress = this.finnAddressRepository
					.findFinnAddressByAreaName(address.getDistrict());
			if (listFinnAddress.size() > 0) {
				for(int i = 0 ; i < listFinnAddress.size() ; i++) {
					if (listFinnAddress.get(i).getCityName().equals(address.getProvince())) {		
							address.setDistrict(listFinnAddress.get(i).getAreaCode());
							address.setProvince(listFinnAddress.get(i).getPostCode());
							addresses.add(address);
					}
				}

			}
		});
		respone.setAddresses(addresses);
		List<FinnAddress> listFinnAddress = this.finnAddressRepository
				.findFinnAddressByCityName(respone.getIssuePlace());
		if (listFinnAddress.size() != 0)
			respone.setIssuePlace(listFinnAddress.get(0).getPostCode());
		this.fptLogRepository.save(FptLog.builder().method("geACCA").custId(cust_id)
				.data("data : " + objectMapper.writeValueAsString(respone)).build());
		return ok(respone);
	}

	@PostMapping("/{router_type}")
	public ResponseEntity<?> addCustomer(@PathVariable("router_type") RouterType router_type,
			@RequestBody @Valid CustomerRequest request)
			throws FptMethodArgumentNotValidException, MethodArgumentNotValidException, JsonProcessingException {
		this.fptLogRepository.save(FptLog.builder().method("addCustomer").custId(request.getCustId())
				.data("data in : " + objectMapper.writeValueAsString(request)).build());
		Optional<FptCustomer> opFptCustomer = this.fptCustomerRepository
				.findFptCustomerByFptCustId(request.getCustId());
		if (!opFptCustomer.equals(Optional.empty()))
			throw new FptExistException(request.getCustId());
		List<FinnAddress> listOpFinnAddress = this.finnAddressRepository
				.findFinnAddressByPostCode(request.getIssuePlace());
		if (listOpFinnAddress.size() == 0)
			throw new FptAddressNotFoundException(-1);
		List<AddressRequest> addresses = new ArrayList<AddressRequest>();
		for (int index = 0; index < request.getAddresses().size(); index++) {
			var address = request.getAddresses().get(index);
			List<FinnAddress> listFinnAddress = this.finnAddressRepository
					.findFinnAddressByAreaCode(address.getDistrict());
			if (listFinnAddress.size() > 0) {
				FinnAddress finnAddress = listFinnAddress.get(0);
				if (finnAddress.getPostCode().equals(address.getProvince())) {
					address.setRegion(finnAddress.getRegion());
					address.setDistrict(finnAddress.getAreaName());
					address.setProvince(finnAddress.getCityName());
					addresses.add(address);
				} else {
					throw new FptAddressNotFoundException(index);
				}
			} else {
				throw new FptAddressNotFoundException(index);
			}
		}
		request.setIssuePlace(listOpFinnAddress.get(0).getCityName());
		request.setAddresses(addresses);
		FptCustomer _customerMapper = request.toFptCustomer();
		this.fptLogRepository.save(FptLog.builder().method("addCustomer").custId(request.getCustId())
				.data("data convert : " + objectMapper.writeValueAsString(_customerMapper)).build());
		FptCustomer fptCustomer = this.fptCustomerRepository.save(_customerMapper);
		this.fptLogRepository.save(
				FptLog.builder().method("addCustomer").custId(request.getCustId()).data("success save db").build());
		if (fptCustomer != null) {
			sendToAutomationService(request.getCustId(), objectMapper.writeValueAsString(fptCustomer));
			updateAppStatus(request.getCustId(), AppStatus.PROCESSING);
			sendToNodeService(request.getCustId());
		}
		this.fptStatusLogRepository.save(FptStatusLog.builder().custId(fptCustomer.getCustId()).appId(fptCustomer.getAppId()).status("PROCESSING_ADD").note("ADD NEW").build());
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}

	@PostMapping("/{router_type}/{cust_id}/doc_post_approved")
	public ResponseEntity<?> addDocPostApproved(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id, @RequestBody @Valid List<DocPostApprovedRequest> request)
			throws FptMethodArgumentNotValidException, JsonProcessingException {
		this.fptLogRepository.save(FptLog.builder().method("addDocPostApproved").custId(cust_id)
				.data("data in : " + objectMapper.writeValueAsString(request)).build());
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));
		if (fptCustomer.getStatus() == null || !fptCustomer.getStatus().equals(AppStatus.APPROVED.toString())) {
			this.fptLogRepository.save(FptLog.builder().method("addDocPostApproved").custId(cust_id)
					.data("error : " + cust_id + " cần status phải ở trạng thái là approved!").build());
			throw new FptStatusNotMatchException(cust_id + " cần status phải ở trạng thái là approved!");
		}
		fptCustomer.getDocPostApproved().forEach(x -> fptDocPostApprovedRepository.delete(x));
		request.forEach(doc -> {
			FptDocPostApproved fptDoc = doc.toFptDocPostApproved();
			fptDoc.setFptCustomer(fptCustomer);
			fptDocPostApprovedRepository.save(fptDoc);
		});
		this.fptLogRepository.save(FptLog.builder().method("addDocPostApproved").custId(cust_id)
				.data("success add docpostapprove").build());
		sendToNodeService(cust_id);
		this.fptStatusLogRepository.save(FptStatusLog.builder().custId(fptCustomer.getCustId()).appId(fptCustomer.getAppId()).status("APPROVED_ADD_DOC").note("ADD DOC POST APPROVED").build());
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@PostMapping("/{router_type}/{cust_id}/update_status/{status}")
	public ResponseEntity<?> triggerUpdateStatus(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id, @PathVariable("status") String status,
			@RequestParam(required = true) String access_key) throws Exception {
		if (!access_key.equals("access_key_db"))
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		this.fptLogRepository.save(
				FptLog.builder().method("triggerUpdateStatus").custId(cust_id).data("data in : " + status).build());
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));
		if (!fptCustomer.getAppId().contains("APPL")) {
			this.fptLogRepository.save(
					this.fptLogRepository.save(FptLog.builder().method("triggerUpdateStatus").custId(cust_id).data("Application Id Not Valid").build()));
			throw new FptStatusException("Application Id Not Valid");
		}
		
		if (!status.toUpperCase().trim().matches("^(DISBURSED)$") || !fptCustomer.getStatus().equals("APPROVED")) {
			this.fptLogRepository.save(FptLog.builder().method("triggerUpdateStatus").custId(cust_id).data("Status is not valid").build());
			throw new FptStatusException(cust_id + " status is not valid");
		}
		
		fptCustomer.setStatus(status);
		fptCustomer.setAutomationResult(
				fptCustomer.getAutomationResult().equals("Pass") ? fptCustomer.getAutomationResult() : "Fix Manually");
		fptCustomer = this.fptCustomerRepository.save(fptCustomer);
		this.fptLogRepository.save(FptLog.builder().method("triggerUpdateStatus").custId(cust_id)
				.data("success update status " + status).build());
		if (status.equals(String.valueOf(AppStatus.APPROVED)))
			SyncACCA(fptCustomer);
		sendStatusToFptApi(FptApiStatusRequest.builder().custId(cust_id).status(status).build());
		sendToNodeService(cust_id);
		this.fptStatusLogRepository.save(FptStatusLog.builder().custId(fptCustomer.getCustId()).appId(fptCustomer.getAppId()).status(status).note("ADD STATUS "+status.toUpperCase() + " FROM TRIGGER").build());
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@PostMapping("/{router_type}/{cust_id}")
	public ResponseEntity<?> updateStatus(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id, @RequestBody ObjectNode body) throws Exception {
	
			this.fptLogRepository.save(FptLog.builder().method("updateStatus").custId(cust_id)
					.data("data in : " + objectMapper.writeValueAsString(body)).build());
			FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
					.orElseThrow(() -> new FptNotFoundException(cust_id));
			if (!fptCustomer.getAppId().contains("APPL")) {
				throw new FptStatusException("Application Id Not Valid");
			}

			if (body.get("status").asText().isEmpty()
					|| !body.get("status").asText().trim().toUpperCase()
							.matches("^(APPROVED|CANCELLED|REJECTED|APPROVED_DOCUMENTCHECK)$")
					)
				throw new Exception("Status or Data is required");
			if (fptCustomer.getStatus().matches("^(CANCELLED|REJECTED|RETURNED|SUPPLEMENT)$")
					|| (body.get("status").asText().trim().toUpperCase() == "APPROVED"
							&& fptCustomer.getStatus() != "APPROVED_DOCUMENTCHECK")) {
				throw new FptStatusException("Cann't update status");
			}

			String status = body.get("status").asText().trim().toUpperCase();
			fptCustomer.setStatus(status);
			fptCustomer.setAutomationResult(
					fptCustomer.getAutomationResult().equals("Pass") ? fptCustomer.getAutomationResult()
							: "Fix Manually");

			switch (status) {
				case "APPROVED":
					String data = body.get("data").asText().trim();
					if (data.isEmpty())
						throw new Exception("Status or Data is required");
					SyncACCA(fptCustomer);
					FptLoanDetail fptloanDetail = fptCustomer.getLoanDetail();
					fptloanDetail.setEmi(Double.valueOf(data));
					fptCustomer.setLoanDetail(fptloanDetail);
					break;
				default:
					break;
			}

			fptCustomer = this.fptCustomerRepository.save(fptCustomer);
			sendToNodeService(cust_id);
			this.fptLogRepository.save(FptLog.builder().method("updateStatus").custId(cust_id)
					.data("success update status " + status).build());
			if (!status.equals("APPROVED_DOCUMENTCHECK")) {
				new Thread(() -> {
				try {
					sendStatusToFptApi(FptApiStatusRequest.builder().custId(cust_id).status(status).message("").build());
				} catch (Exception e) {
		
					log.error("sendStatusToFptApi " + cust_id + " " +  status);
				}
				}).start();
			}
			this.fptStatusLogRepository.save(FptStatusLog.builder().custId(fptCustomer.getCustId()).appId(fptCustomer.getAppId()).status(status).note("ADD STATUS "+status.toUpperCase() + " FROM FICO").build());
			return ResponseEntity.status(HttpStatus.OK).build();
		
	}

	@PostMapping("/{router_type}/{cust_id}/returned_for_data_pre_approved")
	public ResponseEntity<?> addRetuenedForDataPreApproved(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id, @RequestBody List<ReturnedRequest> request) throws Exception {
		this.fptLogRepository.save(FptLog.builder().method("addRetuenedForDataPreApproved").custId(cust_id)
				.data("data in : " + objectMapper.writeValueAsString(request)).build());
		if (request.isEmpty()) {
			this.fptLogRepository.save(FptLog.builder().method("addRetuenedForDataPreApproved").custId(cust_id)
					.data("error : không thấy yêu cầu bổ sung thông tin!").build());
			throw new FptReturnedException("không thấy yêu cầu bổ sung thông tin!");
		}
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));

		if (!fptCustomer.getStatus().equals(AppStatus.PROCESSING.toString())) {
			this.fptLogRepository.save(FptLog.builder().method("addRetuenedForDataPreApproved").custId(cust_id)
					.data("error : " + cust_id + " cần status phải ở trạng thái là processing!").build());
			throw new FptReturnedException(cust_id + " cần status phải ở trạng thái là processing!");
		}
		List<FptReturned> listFptReturned = fptCustomer.getReturned();
		List<FptReturned> listPending = listFptReturned.stream().filter(returned -> returned.getIsPending())
				.collect(Collectors.toList());
		if (!listPending.isEmpty()) {
			this.fptLogRepository.save(FptLog.builder().method("addRetuenedForDataPreApproved").custId(cust_id)
					.data("error : " + cust_id + " đang tồn tại yêu cầu cập nhật!").build());
			throw new FptReturnedException(cust_id + " đang tồn tại yêu cầu cập nhật!");
		}
		List<Comment> listComment = new ArrayList<Comment>();
		request.forEach(x -> {
			FptReturned fptReturn = x.tpfAddReturned();
			fptReturn.setFptCustomer(fptCustomer);
			fptReturnedRepository.save(fptReturn);
			listComment.add(Comment.builder().code(x.getCode()).comment(x.getComment()).build());
		});

		fptCustomer.setStatus(AppStatus.RETURNED.toString());
		this.fptCustomerRepository.save(fptCustomer);
		this.fptLogRepository.save(FptLog.builder().method("addRetuenedForDataPreApproved").custId(cust_id)
				.data("success save data and update status return").build());
		sendStatusToFptApi(FptApiStatusRequest.builder().custId(cust_id).status(AppStatus.RETURNED.toString()).build());
		sendCommentToFptApi(CommentsRequestToFptApi.builder().custId(cust_id).errors(listComment).build());
		
		this.fptStatusLogRepository.save(FptStatusLog.builder().custId(fptCustomer.getCustId()).appId(fptCustomer.getAppId()).status("RETURNED").note("ADD STATUS RETURNED COMMENT FROM FICO").build());
		
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@PutMapping("/{router_type}/{cust_id}/returned_for_data_pre_approved")
	public ResponseEntity<?> updateRetuenedForDataPreApproved(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id, @RequestBody List<ReturnedRequest> request)
			throws FptMethodArgumentNotValidException, JsonProcessingException {
		this.fptLogRepository.save(FptLog.builder().method("updateRetuenedForDataPreApproved").custId(cust_id)
				.data("data in : " + objectMapper.writeValueAsString(request)).build());
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));
		if (!fptCustomer.getStatus().equals(AppStatus.RETURNED.toString())) {
			this.fptLogRepository.save(FptLog.builder().method("updateRetuenedForDataPreApproved").custId(cust_id)
					.data("error : " + cust_id + " cần status phải ở trạng thái là returned!").build());
			throw new FptReturnedException(cust_id + " cần status phải ở trạng thái là returned!");
		}
		List<FptReturned> listFptReturned = fptCustomer.getReturned();
		if (listFptReturned.isEmpty()) {
			this.fptLogRepository.save(FptLog.builder().method("updateRetuenedForDataPreApproved").custId(cust_id)
					.data("error : " + cust_id + " không tồn tại yêu cầu bổ sung thông tin!").build());
			throw new FptReturnedException(cust_id + " không tồn tại yêu cầu bổ sung thông tin!");
		}

		List<FptReturned> listPending = listFptReturned.stream().filter(returned -> returned.getIsPending())
				.collect(Collectors.toList());
		if (listPending.size() != request.size() || !CheckListReturned(listPending, request)) {
			this.fptLogRepository
					.save(FptLog.builder().method("updateRetuenedForDataPreApproved").custId(cust_id)
							.data("error : " + cust_id
									+ " danh sách thông tin bổ sung gửi qua không khớp với danh sách yêu cầu!")
							.build());
			throw new FptReturnedException("Danh sách thông tin bổ sung gửi qua không khớp với danh sách yêu cầu!");
		}

		for (FptReturned returnedPending : listPending) {
			for (ReturnedRequest returned : request) {
				if (returnedPending.getCode().equals(returned.getCode())) {
					returnedPending.setCommentFpt(returned.getComment());
					returnedPending.setIsPending(false);
					fptReturnedRepository.save(returnedPending);
				}
			}
		}
		fptCustomer.setStatus(AppStatus.PROCESSING.toString());
		this.fptCustomerRepository.save(fptCustomer);
		this.fptLogRepository.save(FptLog.builder().method("updateRetuenedForDataPreApproved").custId(cust_id)
				.data("success save data and update status processing").build());
		sendToNodeService(cust_id);
		this.fptStatusLogRepository.save(FptStatusLog.builder().custId(fptCustomer.getCustId()).appId(fptCustomer.getAppId()).status("PROCESSING").note("UPDATE STATUS RETURNED COMMENT FROM FPT").build());
		
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@PostMapping("/{router_type}/{cust_id}/supplement_for_document_post_approved")
	public ResponseEntity<?> addSupplementForDocumentPostApproved(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id, @RequestBody List<SupplementRequest> request) throws Exception {
		this.fptLogRepository.save(FptLog.builder().method("addSupplementForDocumentPostApproved").custId(cust_id)
				.data("data in : " + objectMapper.writeValueAsString(request)).build());
		if (request.isEmpty()) {
			this.fptLogRepository.save(FptLog.builder().method("addSupplementForDocumentPostApproved").custId(cust_id)
					.data("error :  không thấy yêu cầu bổ sung thông tin!").build());
			throw new FptReturnedException("không thấy yêu cầu bổ sung thông tin!");
		}
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));
		if (!fptCustomer.getStatus().equals(AppStatus.APPROVED.toString())) {
			this.fptLogRepository.save(FptLog.builder().method("addSupplementForDocumentPostApproved").custId(cust_id)
					.data("error : " + cust_id + " cần status phải ở trạng thái là approved!").build());
			throw new FptSupplementException(cust_id + " cần status phải ở trạng thái là approved!");
		}
		List<FptSupplement> listFptSupplement = fptCustomer.getSupplement();
		List<FptSupplement> listPending = listFptSupplement.stream().filter(returned -> returned.getIsPending())
				.collect(Collectors.toList());
		if (!listPending.isEmpty()) {
			this.fptLogRepository.save(FptLog.builder().method("addSupplementForDocumentPostApproved").custId(cust_id)
					.data("error : " + cust_id + " đang tồn tại yêu cầu cập nhật!").build());
			throw new FptSupplementException(cust_id + " đang tồn tại yêu cầu cập nhật!");
		}
		List<Comment> listComment = new ArrayList<Comment>();
		request.forEach(x -> {
			FptSupplement fptSupplement = x.tpfAddSupplement();
			fptSupplement.setFptCustomer(fptCustomer);
			fptSupplementRepository.save(fptSupplement);
			listComment.add(Comment.builder().code(x.getCode()).comment(x.getComment()).build());
		});
		fptCustomer.setStatus(AppStatus.SUPPLEMENT.toString());
		this.fptCustomerRepository.save(fptCustomer);
		this.fptLogRepository.save(FptLog.builder().method("addSupplementForDocumentPostApproved").custId(cust_id)
				.data("success save data and update status supplement").build());
		sendStatusToFptApi(
				FptApiStatusRequest.builder().custId(cust_id).status(AppStatus.SUPPLEMENT.toString()).build());
		sendCommentToFptApi(CommentsRequestToFptApi.builder().custId(cust_id).errors(listComment).build());
		this.fptStatusLogRepository.save(FptStatusLog.builder().custId(fptCustomer.getCustId()).appId(fptCustomer.getAppId()).status("SUPPLEMENT").note("ADD STATUS SUPPLEMENT COMMENT FROM FICO").build());
		
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@PutMapping("/{router_type}/{cust_id}/supplement_for_document_post_approved")
	public ResponseEntity<?> updateSupplementForDocumentPostApproved(
			@PathVariable("router_type") RouterType router_type, @PathVariable("cust_id") Long cust_id,
			@RequestBody List<SupplementRequest> request)
			throws FptMethodArgumentNotValidException, JsonProcessingException {
		this.fptLogRepository.save(FptLog.builder().method("updateSupplementForDocumentPostApproved").custId(cust_id)
				.data("data in : " + objectMapper.writeValueAsString(request)).build());
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));
		if (!fptCustomer.getStatus().equals(AppStatus.SUPPLEMENT.toString())) {
			this.fptLogRepository
					.save(FptLog.builder().method("updateSupplementForDocumentPostApproved").custId(cust_id)
							.data("error : " + cust_id + " cần status phải ở trạng thái là supplement!").build());
			throw new FptSupplementException(cust_id + " cần status phải ở trạng thái là supplement!");
		}
		List<FptSupplement> listFptSupplement = fptCustomer.getSupplement();
		if (listFptSupplement.isEmpty()) {
			this.fptLogRepository.save(FptLog.builder().method("updateSupplementForDocumentPostApproved")
					.custId(cust_id).data("error : " + cust_id + " không tồn tại yêu cầu bổ sung thông tin!").build());
			throw new FptSupplementException(cust_id + " không tồn tại yêu cầu bổ sung thông tin!");
		}
		List<FptSupplement> listPending = listFptSupplement.stream().filter(returned -> returned.getIsPending())
				.collect(Collectors.toList());
		if (listPending.size() != request.size() || !CheckListSupplement(listPending, request)) {
			this.fptLogRepository.save(FptLog.builder().method("updateSupplementForDocumentPostApproved")
					.custId(cust_id)
					.data("error : danh sách thông tin bổ sung gửi qua không khớp với danh sách yêu cầu!").build());
			throw new FptReturnedException("Danh sách thông tin bổ sung gửi qua không khớp với danh sách yêu cầu!");
		}
		for (FptSupplement supplementPending : listPending) {
			for (SupplementRequest supplement : request) {
				if (supplementPending.getCode().equals(supplement.getCode())) {
					supplementPending.setCommentFpt(supplement.getComment());
					supplementPending.setIsPending(false);
					fptSupplementRepository.save(supplementPending);
				}
			}

		}
		fptCustomer.setStatus(AppStatus.APPROVED.toString());
		this.fptCustomerRepository.save(fptCustomer);
		this.fptLogRepository.save(FptLog.builder().method("updateSupplementForDocumentPostApproved").custId(cust_id)
				.data("success save data and update status approved").build());
		sendToNodeService(cust_id);
		this.fptStatusLogRepository.save(FptStatusLog.builder().custId(fptCustomer.getCustId()).appId(fptCustomer.getAppId()).status("APPROVED").note("UPDATED STATUS SUPPLEMENT COMMENT FROM FPT").build());

		return ResponseEntity.status(HttpStatus.OK).build();
	}

	@PostMapping("/{router_type}/{cust_id}/{app_id}/update_automation_result")
	public ResponseEntity<?> updateAutomationResult(@PathVariable("router_type") RouterType router_type,
			@PathVariable("cust_id") Long cust_id, @PathVariable("app_id") String app_id,
			@RequestParam(required = true) String automation_result, @RequestParam(required = true) String access_key)
			throws Exception {
		if (!access_key.equals("access_key_db"))
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		this.fptLogRepository.save(FptLog.builder().method("updateAutomationResult").custId(cust_id)
				.data("data in : " + automation_result).build());
		FptCustomer fptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id)
				.orElseThrow(() -> new FptNotFoundException(cust_id));
		fptCustomer.setAutomationResult(automation_result);
		fptCustomer.setAppId(app_id);
		this.fptCustomerRepository.save(fptCustomer);
		this.fptLogRepository.save(
				FptLog.builder().method("updateAutomationResult").custId(cust_id).data("success save data").build());
		sendToNodeService(cust_id);
		this.fptStatusLogRepository.save(FptStatusLog.builder().custId(cust_id).appId(app_id).status("PROCESSING_AUTOMATION_UPDATE").note(automation_result).build());
		return ResponseEntity.status(HttpStatus.OK).build();
	}

	private void SyncACCA(FptCustomer currentFptCustomer) throws JsonProcessingException {
		this.fptLogRepository.save(FptLog.builder().method("SyncACCA").custId(currentFptCustomer.getCustId())
				.data("data in : " + objectMapper.writeValueAsString(currentFptCustomer)).build());
		try {
			Map<String, Object> accaResult = getAccaWithAppId(currentFptCustomer.getCustId(),
					currentFptCustomer.getAppId());
			this.fptLogRepository.save(FptLog.builder().method("SyncACCA").custId(currentFptCustomer.getCustId())
					.data("data get from finn  : " + objectMapper.writeValueAsString(accaResult)).build());
			if (Integer.parseInt(accaResult.get("result").toString()) == 0) {
				var _customerDb = accaResult.get("data");
				@SuppressWarnings("unchecked")
				Map<String, Object> _mapCustomer = objectMapper.convertValue(_customerDb, Map.class);
				var _fptcustomerDb = objectMapper.convertValue(_customerDb, CustomerRequest.class);
				var _fptCustomer = _fptcustomerDb.toFptCustomer();
				_fptCustomer.setFirstName(_fptcustomerDb.getFirstName());
				_fptCustomer.setLastName(_fptcustomerDb.getLastName());
				_fptCustomer.setAppId(_mapCustomer.get("appId").toString());
				_fptCustomer.setId(currentFptCustomer.getId());
				_fptCustomer.setStatus(currentFptCustomer.getStatus());
				_fptCustomer.setAutomationResult(currentFptCustomer.getAutomationResult());
				_fptCustomer.setIssuePlace(_fptCustomer.getIssuePlace().trim());
				_fptCustomer.setCreatedDate(currentFptCustomer.getCreatedDate());
				_fptCustomer.setPhotos(currentFptCustomer.getPhotos());
				_fptCustomer.setDocPostApproved(currentFptCustomer.getDocPostApproved());
				_fptCustomer.setReturned(currentFptCustomer.getReturned());
				_fptCustomer.setDocPostApproved(currentFptCustomer.getDocPostApproved());
				_fptCustomer.getLoanDetail().setDueDate(null);
				this.fptLogRepository.save(FptLog.builder().method("SyncACCA").custId(currentFptCustomer.getCustId())
						.data("data convert finn to fpt : " + objectMapper.writeValueAsString(_fptCustomer)).build());
				fptCustomerRepository.save(_fptCustomer);
				this.fptLogRepository.save(FptLog.builder().method("SyncACCA").custId(currentFptCustomer.getCustId())
						.data("success save data").build());
			}
		} catch (Exception e) {
			this.fptLogRepository.save(FptLog.builder().method("SyncACCA").custId(currentFptCustomer.getCustId())
					.data("error : " + e.getMessage()).build());
		}
	}

	private Boolean CheckListReturned(List<FptReturned> listFptReturned, List<ReturnedRequest> listReturnedRequest) {
		Boolean result = false;
		for (ReturnedRequest returnedRequest : listReturnedRequest) {
			result = false;
			for (FptReturned fptReturned : listFptReturned) {
				if (fptReturned.getIsPending() && returnedRequest.getCode().equals(fptReturned.getCode())) {
					result = true;
					log.info(fptReturned.getCode());
					break;
				}
			}
		}
		return result;
	}

	private Boolean CheckListSupplement(List<FptSupplement> listFptSupplement,
			List<SupplementRequest> listSupplementRequest) {
		Boolean result = false;
		for (SupplementRequest supplementRequest : listSupplementRequest) {
			result = false;
			for (FptSupplement fptSupplement : listFptSupplement) {
				if (fptSupplement.getIsPending() && supplementRequest.getCode().equals(supplementRequest.getCode())) {
					result = true;
					log.info(fptSupplement.getCode());
					break;
				}
			}
		}
		return result;
	}

	private void sendToNodeService(Long cust_id) {
		final String url = urlNodeService + "/refresh_data/" + cust_id;
		try {
			ResponseEntity<Object> result = restTemplate.postForEntity(url, null, Object.class);
			if (!result.getStatusCode().equals(HttpStatus.OK))
				this.fptLogRepository.save(FptLog.builder().method("sendToNodeService").custId(cust_id)
						.data("success send to web").build());
		} catch (Exception e) {
			this.fptLogRepository.save(FptLog.builder().method("sendToNodeService").custId(cust_id)
					.data("error : " + e.getMessage()).build());
		}
	}

	private Map<String, Object> getAccaWithAppId(Long cust_id, String applicationId) {
		try {
			final String url = "http://" + localhost + ":8080/fpt/acca/" + applicationId + "?access_key=access_key_db";
			@SuppressWarnings("unchecked")
			Map<String, Object> data = restTemplate.getForObject(url, Map.class);
			this.fptLogRepository.save(FptLog.builder().method("getAccaWithAppId").custId(cust_id)
					.data("data result : " + objectMapper.writeValueAsString(data)).build());
			return data;
		} catch (Exception e) {
			log.error(e.getMessage());
			this.fptLogRepository.save(FptLog.builder().method("getAccaWithAppId").custId(cust_id)
					.data("error : " + e.getMessage()).build());
			return null;
		}
	}

	private void sendToAutomationService(Long cust_id, String customer) {
		final String url = urlAutomationService + ":8484/fpt/customers_fpt_test";
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
			this.fptLogRepository.save(FptLog.builder().method("sendToAutomationService").custId(cust_id)
					.data("data in : " + customer).build());
			HttpEntity<?> request = new HttpEntity<>(customer, headers);
			ResponseEntity<Object> result = restTemplate.postForEntity(url, request, Object.class);
			this.fptLogRepository.save(FptLog.builder().method("sendToAutomationService").custId(cust_id)
					.data("api result : " + result.getStatusCode().toString()).build());
		} catch (Exception e) {
			log.error("automation api exception : " + e.getMessage());
			this.fptLogRepository.save(FptLog.builder().method("sendToAutomationService").custId(cust_id)
					.data("error : " + e.getMessage()).build());
		}
	}

	private void sendStatusToFptApi(FptApiStatusRequest fptApiStatusRequest) throws Exception {
		final String url = urlFptApi + "/RegistrationForm/InstallmentTPBank_TPFico_PushResult";
		String data = objectMapper.writeValueAsString(fptApiStatusRequest);
		this.fptLogRepository.save(FptLog.builder().method("sendStatusToFptApi").custId(fptApiStatusRequest.getCustId())
				.data("data in : " + data).build());
		try {
			PGPHelper pgpHelper = new PGPHelper();
			ByteArrayOutputStream encStream = new ByteArrayOutputStream();
			pgpHelper.encryptAndSign(data.getBytes(), encStream);
			this.fptLogRepository.save(FptLog.builder().method("sendStatusToFptApi")
					.custId(fptApiStatusRequest.getCustId()).data("data in enc : " + encStream.toString()).build());
			ResponseEntity<Object> result = restTemplate.postForEntity(url, encStream.toString(), Object.class);
			this.fptLogRepository.save(FptLog.builder().method("sendStatusToFptApi")
					.custId(fptApiStatusRequest.getCustId())
					.data("api resutl : " + result.getStatusCode().toString() + " " + result.getBody().toString())
					.build());

		} catch (Exception e) {
			log.info("fpt api send status exception : " + e.getMessage());
			this.fptLogRepository.save(FptLog.builder().method("sendStatusToFptApi")
					.custId(fptApiStatusRequest.getCustId()).data("error : " + e.getMessage()).build());
		}
	}

	private void sendCommentToFptApi(CommentsRequestToFptApi commentsRequestToFptApi) throws Exception {
		final String url = urlFptApi + "/RegistrationForm/InstallmentTPBank_TPFico_PushResult_UpdateDetail";
		String data = objectMapper.writeValueAsString(commentsRequestToFptApi);
		this.fptLogRepository.save(FptLog.builder().method("sendCommentToFptApi")
				.custId(commentsRequestToFptApi.getCustId()).data("data in : " + data).build());
		try {
			PGPHelper pgpHelper = new PGPHelper();
			ByteArrayOutputStream encStream = new ByteArrayOutputStream();
			pgpHelper.encryptAndSign(data.getBytes(), encStream);
			this.fptLogRepository.save(FptLog.builder().method("sendCommentToFptApi")
					.custId(commentsRequestToFptApi.getCustId()).data("data in enc : " + encStream.toString()).build());

			ResponseEntity<Object> result = restTemplate.postForEntity(url, encStream.toString(), Object.class);
			this.fptLogRepository.save(FptLog.builder().method("sendCommentToFptApi")
					.custId(commentsRequestToFptApi.getCustId())
					.data("api resutl : " + result.getStatusCode().toString() + " " + result.getBody().toString())
					.build());

		} catch (Exception e) {
			log.info("fpt api comment exception : " + e.getMessage());
			this.fptLogRepository.save(FptLog.builder().method("sendCommentToFptApi")
					.custId(commentsRequestToFptApi.getCustId()).data("error: " + e.getMessage()).build());
		}

	}

	private void updateAppStatus(long cust_id, AppStatus appstatus) {
		Optional<FptCustomer> opFptCustomer = this.fptCustomerRepository.findFptCustomerByFptCustId(cust_id);
		if (opFptCustomer.equals(Optional.empty()))
			throw new FptNotFoundException(cust_id);
		FptCustomer fptCustomer = opFptCustomer.get();
		fptCustomer.setStatus(appstatus.toString());
		this.fptCustomerRepository.save(fptCustomer);
	}

	private enum RouterType {
		customers_json, customers_pgp
	}

	public enum AppStatus {
		PROCESSING, APPROVED, DISBURSED, CANCELLED, REJECTED, RETURNED, SUPPLEMENT
	}

	@Builder
	@Getter
	private static class FptApiStatusRequest {
		private long custId;
		private String status;
		private String message;
	}

	@Builder
	@Getter
	private static class CommentsRequestToFptApi {
		private long custId;
		private List<Comment> errors;
	}

	@Builder
	@Getter
	private static class Comment {
		private String code;
		private String comment;
	}

//	@PutMapping("/{momo_loan_id}")
//	public ResponseEntity<?> update(@PathVariable("momo_loan_id") Integer momo_loan_id,
//			@RequestBody @Valid ApiMomoRequest form) throws MomoMethodArgumentNotValidException {
//
//		MomoCustomer opMomoCustomer = this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		MomoCustomer customer = form.toMomoCustomer();
//		customer.setId(opMomoCustomer.getId());
//		this.momoCustomerRepository.save(customer);
//		return noContent().build();
//	}

//	@DeleteMapping("/{momo_loan_id}")
//	public ResponseEntity<?> delete(@PathVariable("momo_loan_id") Integer momo_loan_id) {
//		MomoCustomer existed = this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		this.momoCustomerRepository.delete(existed);
//		return noContent().build();
//	}

//
//	@GetMapping("/{momo_loan_id}/photos/{photo_id}")
//	public ResponseEntity<?> getPhoto(@PathVariable("momo_loan_id") Integer momo_loan_id,
//			@PathVariable("photo_id") Long photo_id) {
//		this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		MomoPhoto momoPhoto = this.momoPhotoRepository.findById(photo_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoPhoto.class.getSimpleName(), photo_id));
//		return ok(momoPhoto);
//
//	}
//
//	@PostMapping("/{momo_loan_id}/photos")
//	public ResponseEntity<?> savePhoto(@PathVariable("momo_loan_id") Integer momo_loan_id,
//			@RequestBody @Valid MomoPhotoRequest form) throws MomoMethodArgumentNotValidException {
//		MomoCustomer momoCustomer = this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		MomoPhoto momoPhoto = form.toMomoPhoto();
//		momoPhoto.setMomoCustomer(momoCustomer);
//		return ResponseEntity.status(HttpStatus.CREATED).body(this.momoPhotoRepository.save(momoPhoto));
//
//	}
//
//	@PutMapping("/{momo_loan_id}/photos/{photo_id}")
//	public ResponseEntity<?> updatePhoto(@PathVariable("momo_loan_id") Integer momo_loan_id,
//			@PathVariable("photo_id") Long photo_id, @RequestBody @Valid MomoPhoto form)
//			throws MomoMethodArgumentNotValidException {
//		this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		MomoPhoto momoPhoto = this.momoPhotoRepository.findById(photo_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoPhoto.class.getSimpleName(), photo_id));
//		form.setCreatedDate(momoPhoto.getCreatedDate());
//		this.momoPhotoRepository.save(form);
//		return noContent().build();
//	}
//
//	@DeleteMapping("/{momo_loan_id}/photos/{photo_id}")
//	public ResponseEntity<?> deletePhoto(@PathVariable("momo_loan_id") Integer momo_loan_id,
//			@PathVariable("photo_id") Long photo_id) {
//		this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		MomoPhoto existed = this.momoPhotoRepository.findById(photo_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoPhoto.class.getSimpleName(), photo_id));
//		this.momoPhotoRepository.delete(existed);
//		return noContent().build();
//	}
//
//	@GetMapping("/{id}/references")
//	public ResponseEntity<?> allRelationship(@PathVariable("id") Long id) {
//		return ResponseEntity.ok(this.momoReferenceRepository.findAllByCustomer(id));
//
//	}
//
//	@GetMapping("/{momo_loan_id}/references/{reference_id}")
//	public ResponseEntity<?> getReferences(@PathVariable("momo_loan_id") Integer momo_loan_id,
//			@PathVariable("reference_id") Long reference_id) {
//		this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		MomoReference momoReference = this.momoReferenceRepository.findById(reference_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoReference.class.getSimpleName(), reference_id));
//		return ok(momoReference);
//
//	}
//
//	@PostMapping("/{momo_loan_id}/references")
//	public ResponseEntity<?> saveRelationship(@PathVariable("momo_loan_id") Integer momo_loan_id,
//			@RequestBody MomoReferenceRequest form) {
//		this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		MomoReference reference = form.toMomoReference();
//		return ResponseEntity.status(HttpStatus.CREATED).body(this.momoReferenceRepository.save(reference));
//	}
//
//	@PutMapping("/{momo_loan_id}/references/{reference_id}")
//	public ResponseEntity<?> updateRelationship(@PathVariable("momo_loan_id") Integer momo_loan_id,
//			@PathVariable("reference_id") Long reference_id, @RequestBody @Valid MomoReferenceRequest form)
//			throws MomoMethodArgumentNotValidException {
//		this.momoCustomerRepository.findMomoCustomerByMomoLoanId(momo_loan_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), momo_loan_id));
//		this.momoReferenceRepository.findById(reference_id)
//				.orElseThrow(() -> new MomoNotFoundException(MomoReference.class.getSimpleName(), reference_id));
//		MomoReference reference = form.toMomoReference();
//		this.momoReferenceRepository.save(reference);
//		return noContent().build();
//	}
//	

//      @DeleteMapping("/{id}/relationship/{relationship_id}")
//      public ResponseEntity<?> deleteRelationship(@PathVariable("id") Long id,
//                  @PathVariable("relationship_id") Long relationship_id) {
//            this.momoCustomerRepository.findById(id)
//                        .orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), id));
//            MomoRelationship existed = this.momoRelationshipRepository.findById(relationship_id).orElseThrow(
//                        () -> new MomoNotFoundException(MomoRelationship.class.getSimpleName(), relationship_id));
//            this.momoRelationshipRepository.delete(existed);
//            return noContent().build();
//      }
//
//     
//      @GetMapping("/{id}/currentaddress")
//      public ResponseEntity<?> allAddress(@PathVariable("id") Long id) {
//            MomoCustomer momoCustomer = this.momoCustomerRepository.findById(id)
//                        .orElseThrow(() -> new MomoNotFoundException(MomoCustomer.class.getSimpleName(), id));
//            return ResponseEntity.ok(momoCustomer.getCurrentAddress());
//      }

}
