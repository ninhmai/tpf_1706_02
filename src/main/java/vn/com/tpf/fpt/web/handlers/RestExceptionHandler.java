package vn.com.tpf.fpt.web.handlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.ValidationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;

import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import lombok.extern.slf4j.Slf4j;
import vn.com.tpf.fpt.web.exception.AutomationServiceException;
import vn.com.tpf.fpt.web.exception.FptAddressNotFoundException;
import vn.com.tpf.fpt.web.exception.FptExistException;
import vn.com.tpf.fpt.web.exception.FptMethodArgumentNotValidException;
import vn.com.tpf.fpt.web.exception.FptNotFoundException;
import vn.com.tpf.fpt.web.exception.FptReturnedException;
import vn.com.tpf.fpt.web.exception.FptStatusException;
import vn.com.tpf.fpt.web.exception.FptStatusNotMatchException;
import vn.com.tpf.fpt.web.exception.NodeServiceException;
import vn.com.tpf.fpt.web.response.ApiInfoRespone;

/**
 * Created by NgocIT on Feb, 2019
 */

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {


	@ExceptionHandler(AccessDeniedException.class)
	public ResponseEntity<?> accessDeniedException(AccessDeniedException ex, WebRequest request) {
		log.debug("handling accessDeniedException...");
		return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
				.body(ApiInfoRespone.builder().messages("Access Denied").build());
	}

	@ExceptionHandler(value = { MethodArgumentTypeMismatchException.class })
	public ResponseEntity<?> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex,
			WebRequest request) {
		log.error("handling valid  MethodArgumentTypeMismatchException Api not support");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages("Method Argument Type Mismatch").build());
	}

	@ExceptionHandler(value = { HttpMediaTypeNotSupportedException.class })
	public ResponseEntity<?> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException ex,
			WebRequest request) {
		log.error("handling  valid HttpMediaTypeNotSupportedException");
		return ResponseEntity.status(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
				.body(ApiInfoRespone.builder().messages("Http Media Type Not Supported").build());
	}

	@ExceptionHandler(value = { HttpMessageNotReadableException.class })
	public ResponseEntity<?> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex,
			WebRequest request) {
		log.error("handling valid Fpt json parse error " + ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(" Json Object Error").build());
	}

	@ExceptionHandler(value = { FptAddressNotFoundException.class })
	public ResponseEntity<?> handleFptAddressNotFoundException(FptAddressNotFoundException ex, WebRequest request) {
		log.error("handling  valid Fpt Validation Address " +ex.index+" Failed ");
		String error  = ex.index == -1 ? "issuePlace" : "address[" +ex.index + "]";
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ApiInfoRespone.builder()
				.messages("Method Argument Not Valid").errors( Arrays.asList(error)).build());
	}

	@ExceptionHandler(value = { MethodArgumentNotValidException.class })
	public ResponseEntity<?> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, WebRequest request) {
		log.error("handling  valid Fpt Validation Failed");
		List<String> listErrors = new ArrayList<String>();
		for (FieldError error : ex.getBindingResult().getFieldErrors())
			listErrors.add(error.getField());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages("Method Argument Not Valid").errors(listErrors).build());
	}

	@ExceptionHandler(value = { FptExistException.class })
	public ResponseEntity<?> fptExistException(FptExistException ex, WebRequest request) {
		log.error("handling  valid Fpt Exist");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(ex.getMessage()).build());
	}

	@ExceptionHandler(value = { FptNotFoundException.class })
	public ResponseEntity<?> fptNotFoundException(FptNotFoundException ex, WebRequest request) {
		log.error("handling valid Fpt information not found");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(ex.getMessage()).build());
	}

	@ExceptionHandler(value = { FptStatusNotMatchException.class })
	public ResponseEntity<?> fptNotFoundException(FptStatusNotMatchException ex, WebRequest request) {
		log.error("handling valid Fpt app  status not match");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(ex.getMessage()).build());
	}

	@ExceptionHandler(value = { FptReturnedException.class })
	public ResponseEntity<?> fptFptReturnedException(FptReturnedException ex, WebRequest request) {
		log.error("handling valid Fpt dont have returned pending");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(ex.getMessage()).build());
	}

	@ExceptionHandler(value = { NodeServiceException.class })
	public ResponseEntity<?> nodeServiceException(NodeServiceException ex, WebRequest request) {
		log.error("handling valid Node Servicec Exception");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(ex.getMessage()).build());
	}

	@ExceptionHandler(value = { AutomationServiceException.class })
	public ResponseEntity<?> automationServiceException(AutomationServiceException ex, WebRequest request) {
		log.error("handling valid Automation Servicec Exception");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(ex.getMessage()).build());
	}

	@ExceptionHandler(value = { ValidationException.class })
	public ResponseEntity<?> validateException(ValidationException ex, WebRequest request) {
		log.error("handling valid Exception information not found");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages("Validation Error").build());
	}

	@ExceptionHandler(value = { FptMethodArgumentNotValidException.class })
	public ResponseEntity<?> fptValidateEException(FptMethodArgumentNotValidException ex, WebRequest request) {
		log.error("handling valid Fpt Method Argument Not Valid");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages("Fpt Method Argument Not Valid").build());
	}
	
	@ExceptionHandler(value = { FptStatusException.class })
	public ResponseEntity<?> fptStatusException(Exception ex, WebRequest request) {
		log.error("handling valid FptStatusException ");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(ex.getMessage()).build());
	}

	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<?> exception(Exception ex, WebRequest request) {
		log.error("handling valid Exception ");
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(ApiInfoRespone.builder().messages(ex.getMessage()).build());
	}
	
	
	
	
	

}
