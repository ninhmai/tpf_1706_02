package vn.com.tpf.fpt.web.exception;

public class NodeServiceException  extends BaseException {

	private static final long serialVersionUID = 2715363440500672381L;

	
	public NodeServiceException( long id) {
		super("Lỗi gọi dịch vụ web cust id " + id + " !");
	}

//	public MomoExistException(String requestId,String referenceId, String entity, Integer momo_loan_id) {
//		super(requestId,referenceId,entity.toUpperCase() + " Momo Loan Id " + momo_loan_id + " Exist.");
//	}
//
//	public MomoExistException(String requestId,String referenceId,String entity, long id) {
//		super(requestId,referenceId,entity.toUpperCase() + " : " + id + " Exist.");
//	}
}
