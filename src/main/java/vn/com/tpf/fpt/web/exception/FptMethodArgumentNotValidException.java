package vn.com.tpf.fpt.web.exception;

import org.springframework.core.MethodParameter;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;

/**
 * MomoMethodArgumentNotValidException
 */
public class FptMethodArgumentNotValidException extends MethodArgumentNotValidException {

	private static final long serialVersionUID = -1799021156311709833L;

	public FptMethodArgumentNotValidException(MethodParameter parameter, BindingResult bindingResult) {
		super(parameter, bindingResult);
	}

}