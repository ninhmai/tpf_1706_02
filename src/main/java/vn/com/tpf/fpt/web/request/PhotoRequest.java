package vn.com.tpf.fpt.web.request;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.Getter;
import vn.com.tpf.fpt.domain.FptPhoto;

@Getter
public class PhotoRequest extends BaseRequest {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull
	private PhotoTypes documentType;

	@NotEmpty
	@Pattern(regexp = "^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)?[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z0-9]{2,5}(:[0-9]{1,5})?(\\/.*)?$")
	private String link;

	public FptPhoto toFptPhoto() {
		return FptPhoto.builder().documentType(documentType.toValue()).link(link).build();
	}

	private enum PhotoTypes {

		National_ID, selfie, employeecard, fb, ACCA, delivery_note, detail_of_stock, Signature;

		private static Map<String, PhotoTypes> namesMap = new HashMap<String, PhotoTypes>(8);

		static {
			namesMap.put("National_ID", National_ID);
			namesMap.put("selfie", selfie);
			namesMap.put("employeecard", employeecard);
			namesMap.put("fb", fb);
			namesMap.put("ACCA", ACCA);
			namesMap.put("delivery_note", delivery_note);
			namesMap.put("detail_of_stock", detail_of_stock);
			namesMap.put("Signature", Signature);

		}

		@JsonCreator
		public static PhotoTypes forValue(String value) {
			return namesMap.get(value);
		}

		@JsonValue
		public String toValue() {
			for (Map.Entry<String, PhotoTypes> entry : namesMap.entrySet()) {
				if (entry.getValue() == this)
					return entry.getKey();
			}
			return null;// or fail
		}

	}
}
