package vn.com.tpf.fpt.repositories;



import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.com.tpf.fpt.domain.FptDocPostApproved;



/**
 * Created by NgocIT on Feb, 2019
 */

@Repository
@Transactional
public interface FptDocPostApprovedRepository extends JpaRepository<FptDocPostApproved, Long> {
	
}