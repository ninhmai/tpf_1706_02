package vn.com.tpf.fpt.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.com.tpf.fpt.domain.FptLog;


@Repository
public interface FptLogRepository extends JpaRepository<FptLog, Long> {
	
	@Query("from FptLog where custId=:custId")
	List<FptLog> findFptLogByCustId(Long custId);

}

