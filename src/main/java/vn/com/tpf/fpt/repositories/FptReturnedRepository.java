package vn.com.tpf.fpt.repositories;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.com.tpf.fpt.domain.FptReturned;



@Repository
@Transactional
public interface FptReturnedRepository extends JpaRepository<FptReturned, Long> {
	
}