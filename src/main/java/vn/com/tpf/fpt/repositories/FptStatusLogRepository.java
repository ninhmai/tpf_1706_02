package vn.com.tpf.fpt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.com.tpf.fpt.domain.FptStatusLog;


@Repository
public interface FptStatusLogRepository extends JpaRepository<FptStatusLog, Long> {

}

