package vn.com.tpf.fpt.repositories;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.com.tpf.fpt.domain.FptCustomer;



/**
 * Created by NgocIT on Feb, 2019
 */

@Repository
public interface FptCustomerRepository extends JpaRepository<FptCustomer, Long> {
	

//	List<FptCustomer> findByCustId(long cust_id);
	
	@Query("from FptCustomer where custId=:cust_id")
	Optional<FptCustomer> findFptCustomerByFptCustId(Long cust_id);
	
	@Query("from FptCustomer where status = :app_status")
	List<FptCustomer> findFptCustomerStatus(String app_status);
	
	@Query("from FptCustomer where appId = :app_id")
	List<FptCustomer> findFptCustomerByAppId(String app_id);
	
	@Query("from FptCustomer where status in :app_status")
	List<FptCustomer> findFptCustomerByListStatus(@Param("app_status") Set<String> app_status);
	
	
	@Query("from FptCustomer where  automationResult != 'Pass' and automationResult != 'Fix Manually'")
	List<FptCustomer> findFptCustomerFailedAutomation();


	@Query("from FptCustomer where automationResult in ('Pass','Fix Manually') and status in ('PROCESSING','RETURNED') and appId like 'APPL%'")
	List<FptCustomer> findFptCustomerForDocumentCheck();
	
	
	@Query("from FptCustomer where  status = 'APPROVED_DOCUMENTCHECK' and automationResult  in ('Pass','Fix Manually') and appId like 'APPL%'")
	List<FptCustomer> findFptCustomerForUnderWriting();
	
	
	@Query("from FptCustomer where  status in ('APPROVED','SUPPLEMENT') and automationResult  in ('Pass','Fix Manually') and appId like 'APPL%'")
	List<FptCustomer> findFptCustomerForLoanCheck();

	
	@Query("from FptCustomer where automationResult in ('Pass','Fix Manually') and appId like 'APPL%' ")
	List<FptCustomer> findFptCustomerPassAutomation();
}