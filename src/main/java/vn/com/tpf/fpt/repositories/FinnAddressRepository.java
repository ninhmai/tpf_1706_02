package vn.com.tpf.fpt.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import vn.com.tpf.fpt.domain.FinnAddress;



@Repository
public interface FinnAddressRepository extends JpaRepository<FinnAddress, Long> {
	
	@Query("from FinnAddress where areaCode=:area_code")
	List<FinnAddress> findFinnAddressByAreaCode(String area_code);
	
	@Query("from FinnAddress where areaName=:area_name")
	List<FinnAddress> findFinnAddressByAreaName(String area_name);
	
	@Query("from FinnAddress where cityName=:city_name")
	List<FinnAddress> findFinnAddressByCityName(String city_name);
	
	@Query("from FinnAddress where postCode=:post_code")
	List<FinnAddress> findFinnAddressByPostCode(String post_code);
	
}
